package org.gcube.informationsystem.resourceregistry.publisher;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.common.gxhttp.reference.GXConnection;
import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.common.http.GXHTTPUtility;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCacheRenewal;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextUtility;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.consistsof.ConsistsOfAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.consistsof.ConsistsOfAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.consistsof.ConsistsOfNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.api.request.BaseRequestInfo;
import org.gcube.informationsystem.resourceregistry.api.rest.AccessPath;
import org.gcube.informationsystem.resourceregistry.api.rest.InstancePath;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath;
import org.gcube.informationsystem.resourceregistry.api.rest.SharingPath.SharingOperation;
import org.gcube.informationsystem.resourceregistry.api.rest.httputils.HTTPUtility;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.TypeUtility;
import org.gcube.informationsystem.utils.UUIDManager;
import org.gcube.informationsystem.utils.UUIDUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceRegistryPublisherImpl extends BaseRequestInfo implements ResourceRegistryPublisher {

	private static final Logger logger = LoggerFactory.getLogger(ResourceRegistryPublisherImpl.class);

	private static final String ACCEPT_HTTP_HEADER_KEY = "Accept";
	private static final String CONTENT_TYPE_HTTP_HEADER_KEY = "Content-Type";
	
	protected final String address;
	
	protected Map<String, String> headers;
	
	protected ContextCache contextCache;
	
	@Override
	public boolean isHierarchicalMode() {
		return hierarchicalMode;
	}
	
	@Override
	public void setHierarchicalMode(boolean hierarchicalMode) {
		this.hierarchicalMode = hierarchicalMode;
	}
	
	@Deprecated
	@Override
	public boolean isIncludeContextsInHeader() {
		return includeContexts();
	}

	@Deprecated
	@Override
	public void setIncludeContextsInHeader(boolean includeContexts) {
		setIncludeContexts(includeContexts);
	}
	
	private void addOptionalQueryParameters(Map<String,String> queryParams) throws UnsupportedEncodingException {
		addHierarchicalMode(queryParams);
		addIncludeContexts(queryParams);
		addIncludeMeta(queryParams);
		addIncludeAllMeta(queryParams);
	}
	
	private GXHTTPStringRequest includeAdditionalQueryParameters(GXHTTPStringRequest gxHTTPStringRequest) throws UnsupportedEncodingException{
		Map<String,String> queryParams = new HashMap<>();
		return includeAdditionalQueryParameters(gxHTTPStringRequest, queryParams);
	}
	
	private GXHTTPStringRequest includeAdditionalQueryParameters(GXHTTPStringRequest gxHTTPStringRequest, Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(queryParams==null) {
			queryParams = new HashMap<>();
		}
		addOptionalQueryParameters(queryParams);
		return gxHTTPStringRequest.queryParams(queryParams);
	}
	
	private void addHierarchicalMode(Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(hierarchicalMode) {
			queryParams.put(InstancePath.HIERARCHICAL_MODE_QUERY_PARAMETER, Boolean.toString(hierarchicalMode));
		}
	}
	
	private void addIncludeContexts(Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(includeContexts) {
			queryParams.put(InstancePath.INCLUDE_CONTEXTS_QUERY_PARAMETER, Boolean.toString(includeContexts));
		}
	}
	
	private void addIncludeMeta(Map<String,String> queryParams) throws UnsupportedEncodingException{
		addIncludeMeta(queryParams, includeMeta);
	}
	
	private void addIncludeMeta(Map<String,String> queryParams, boolean includeMeta) throws UnsupportedEncodingException{
		if(includeMeta) {
			queryParams.put(InstancePath.INCLUDE_META_QUERY_PARAMETER, Boolean.toString(includeMeta));
		}
	}
	
	private void addIncludeAllMeta(Map<String,String> queryParams) throws UnsupportedEncodingException{
		if(allMeta) {
			queryParams.put(InstancePath.INCLUDE_META_IN_ALL_INSTANCES_QUERY_PARAMETER, Boolean.toString(allMeta));
		}
	}
	
	private void addOffset(Map<String,String> queryParams) throws UnsupportedEncodingException{
		addOffset(queryParams, offset);
	}
	
	private void addOffset(Map<String,String> queryParams, Integer offset) throws UnsupportedEncodingException{
		if(offset!=null) {
			queryParams.put(InstancePath.OFFSET_QUERY_PARAMETER, offset.toString());
		}
	}
	
	private void addLimit(Map<String,String> queryParams) throws UnsupportedEncodingException{
		addLimit(queryParams, limit);
	}
	
	private void addLimit(Map<String,String> queryParams, Integer limit) throws UnsupportedEncodingException{
		if(limit!=null) {
			queryParams.put(InstancePath.LIMIT_QUERY_PARAMETER, limit.toString());
		}
	}

	protected ContextCacheRenewal contextCacheRenewal = new ContextCacheRenewal() {
		
		@Override
		public List<Context> renew() throws ResourceRegistryException {
			return getAllContextFromServer(true, 0, BaseRequestInfo.UNBOUNDED_LIMIT);
		}
		
	};
	
	@Override
	public void addHeader(String name, String value) {
		headers.put(name, value);
	}
	
	protected GXHTTPStringRequest getGXHTTPStringRequest() {
		GXHTTPStringRequest gxHTTPStringRequest = GXHTTPUtility.getGXHTTPStringRequest(address);
		gxHTTPStringRequest.from(this.getClass().getSimpleName());
		for(String name : headers.keySet()) {
			gxHTTPStringRequest.header(name, headers.get(name));
		}
		return gxHTTPStringRequest;
	}

	public ResourceRegistryPublisherImpl(String address) {
		this(address, true);
	}
	
	public ResourceRegistryPublisherImpl(String address, boolean sharedContextCache) {
		super();
		this.address = address;
		this.headers = new HashMap<>();
		if(sharedContextCache) {
			contextCache = ContextCache.getInstance();
		}else {
			contextCache = new ContextCache();
		}
		contextCache.setContextCacheRenewal(contextCacheRenewal);
	}

	/**
	 * It reads all the contexts from server. 
	 * The cache used for contexts is bypassed and not updated.
	 * @return All Contexts read from server
	 * @throws ResourceRegistryException
	 */
	public List<Context> getAllContextFromServer() throws ResourceRegistryException {
		return getAllContextFromServer(includeMeta, offset, limit);
	}
	
	protected List<Context> getAllContextFromServer(boolean includeMeta, Integer offset, Integer limit) throws ResourceRegistryException {
		try {
			logger.info("Going to read all {}s", Context.NAME);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.CONTEXTS_PATH_PART);
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters, includeMeta);
			addOffset(parameters, offset);
			addLimit(parameters, limit);
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.debug("Got Contexts are {}", ret);
			return ElementMapper.unmarshalList(Context.class, ret);

		} catch (ResourceRegistryException e) {
			// logger.trace("Error while getting {} schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error while getting {}schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw new RuntimeException(e);
		}
	}
	
	@Deprecated
	@Override
	public List<Context> getAllContext() throws ResourceRegistryException {
		return getContexts();
	}

	@Override
	public List<Context> getContexts() throws ResourceRegistryException {
		return contextCache.getContexts();
	}
	
	@Override
	public ContextCache getContextCache() {
		return contextCache;
	}

	/**
	 * It reads the context from server. 
	 * The cache used for contexts is bypassed and not updated.
	 * @param uuid
	 * @return the Contexts read from server
	 * @throws ContextNotFoundException
	 * @throws ResourceRegistryException
	 */
	public Context getContextFromServer(UUID uuid) throws ContextNotFoundException, ResourceRegistryException {
		return getContextFromServer(uuid.toString());
	}
	
	protected Context getContextFromServer(String uuid) throws ContextNotFoundException, ResourceRegistryException {
		try {
			logger.info("Going to get current {} ", Context.NAME);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.CONTEXTS_PATH_PART);
			gxHTTPStringRequest.path(uuid);

			Map<String,String> parameters = new HashMap<>();
			addIncludeMeta(parameters);
			gxHTTPStringRequest.queryParams(parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			Context context = HTTPUtility.getResponse(Context.class, httpURLConnection);

			logger.debug("Got Context is {}", ElementMapper.marshal(context));
			return context;
		} catch (ResourceRegistryException e) {
			// logger.trace("Error while getting {} schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error while getting {}schema for {}", polymorphic ?
			// AccessPath.POLYMORPHIC_PARAM + " " : "",
			// type, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public Context getContext(UUID uuid) throws ContextNotFoundException, ResourceRegistryException {
		Context context = contextCache.getContextByUUID(uuid);
		if (context == null) {
			context = getContextFromServer(uuid.toString());
			contextCache.cleanCache();
			contextCache.refreshContextsIfNeeded();
			Context c = contextCache.getContextByUUID(uuid);
			if (c != null) {
				context = c;
			} else {
				logger.error(
						"Context with UUID {} is {}. It is possibile to get it from the server but not from the cache. This is very strange and should not occur.",
						uuid, context);
			}
		}
		return context;
	}

	@Override
	public Context getCurrentContext() throws ContextNotFoundException, ResourceRegistryException {
		String contextFullName = org.gcube.common.context.ContextUtility.getCurrentContextFullName();
		UUID uuid = contextCache.getUUIDByFullName(contextFullName);
		return getContext(uuid);
	}

	private UUID getCurrentContextUUID() throws ResourceRegistryException {
		return getCurrentContext().getID();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <ERElem extends ERElement> List<ERElem> list(Class<ERElem> clazz, Boolean polymorphic)
			throws ResourceRegistryException {
		String type = TypeUtility.getTypeName(clazz);
		String ret = list(type, polymorphic);
		try {
			return (List<ERElem>) ElementMapper.unmarshalList(ERElement.class, ret);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String list(String type, Boolean polymorphic) throws ResourceRegistryException {
		try {
			logger.info("Going to get all instances of {} ", type);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			
			Map<String,String> parameters = new HashMap<>();
			addOffset(parameters);
			addLimit(parameters);
			parameters.put(InstancePath.POLYMORPHIC_QUERY_PARAMETER, polymorphic.toString());
			includeAdditionalQueryParameters(gxHTTPStringRequest, parameters);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.debug("Got instances of {} are {}", type, ret);
			return ret;
		} catch(ResourceRegistryException e) {
			// logger.trace("Error while getting {} instances", type, e);
			throw e;
		} catch(Exception e) {
			// logger.trace("Error while getting {} instances", type, e);
			throw new RuntimeException(e);
		}
	}

	protected String create(String type, String json, UUID uuid)
			throws SchemaViolationException, AlreadyPresentException, ResourceRegistryException {
		try {
			logger.trace("Going to create {} : {}", type, json);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.header(CONTENT_TYPE_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(InstancePath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());

			Map<String,String> parameters = new HashMap<>();
			addIncludeContexts(parameters);
			addIncludeMeta(parameters);
			addIncludeAllMeta(parameters);
			gxHTTPStringRequest.queryParams(parameters);
			
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.put(json);
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.trace("{} successfully created", ret);
			return ret;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	protected <ERElem extends ERElement> String internalCreate(ERElem er)
			throws SchemaViolationException, AlreadyPresentException, ResourceRegistryException {
		try {
			String type = er.getTypeName();
			UUID uuid = er.getID();
			if (uuid == null) {
				uuid = UUIDManager.getInstance().generateValidUUID();
				er.setID(uuid);
			}
			String json = ElementMapper.marshal(er);
			return create(type, json, uuid);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <ERElem extends ERElement> ERElem create(ERElem er)
			throws SchemaViolationException, AlreadyPresentException, ResourceRegistryException {
		try {
			String ret = internalCreate(er);
			return (ERElem) ElementMapper.unmarshal(ERElement.class, ret);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public String create(String json)
			throws SchemaViolationException, AlreadyPresentException, ResourceRegistryException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(json);
			String type = TypeUtility.getTypeName(jsonNode);
			UUID uuid = UUIDUtility.getUUID(jsonNode);
			if(uuid==null) {
				uuid = UUIDManager.getInstance().generateValidUUID();
				((ObjectNode) jsonNode).put(ERElement.ID_PROPERTY, uuid.toString());
				json = mapper.writeValueAsString(jsonNode);
			}
			return create(type, json, uuid);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public <ERElem extends ERElement> boolean exist(ERElem er)
			throws AvailableInAnotherContextException, ResourceRegistryException {
		String type = er.getTypeName();
		UUID uuid = er.getID();
		return exist(type, uuid);
	}
	
	@Override
	public <ERElem extends ERElement> boolean exist(Class<ERElem> clazz, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException {
		String type = TypeUtility.getTypeName(clazz);
		return exist(type, uuid);
	}

	@Override
	public boolean exist(String type, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException {
		try {
			logger.info("Going to check if {} with UUID {} exists", type, uuid);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(AccessPath.ACCESS_PATH_PART);
			gxHTTPStringRequest.path(AccessPath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());

			Map<String,String> queryParams =  new HashMap<>();
			addHierarchicalMode(queryParams);
			gxHTTPStringRequest.queryParams(queryParams);

			HttpURLConnection httpURLConnection = gxHTTPStringRequest.head();
			HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.debug("{} with UUID {} exists", type, uuid);
			return true;
		} catch (NotFoundException e) {
			return false;
		} catch (ResourceRegistryException e) {
			// logger.trace("Error while checking if {} with UUID {} exists.", type, uuid,
			// e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error while checking if {} with UUID {} exists.", type, uuid,
			// e);
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <ERElem extends ERElement> ERElem read(ERElem er)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		try {
			String type = er.getTypeName();
			UUID uuid = er.getID();
			String ret = read(type, uuid);
			return (ERElem) ElementMapper.unmarshal(ERElement.class, ret);
		} catch (ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <ERElem extends ERElement> ERElem read(Class<ERElem> clazz, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		try {
			String type = TypeUtility.getTypeName(clazz);
			String ret = read(type, uuid);
			return (ERElem) ElementMapper.unmarshal(ERElement.class, ret);
		} catch (ResourceRegistryException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String read(String type, UUID uuid) 
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException {
		try {
			logger.trace("Going to read {} with UUID {}", type, uuid);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(InstancePath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());

			includeAdditionalQueryParameters(gxHTTPStringRequest);

			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.debug("Got {} with UUID {} is {}", type, uuid, ret);
			return ret;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <ERElem extends ERElement> ERElem update(ERElem er)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException {
		try {
			String type = er.getTypeName();
			String json = ElementMapper.marshal(er);
			UUID uuid = er.getID();
			String ret = update(type, json, uuid);
			return (ERElem) ElementMapper.unmarshal(ERElement.class, ret);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String update(String json)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException {
		try {
			String type = TypeUtility.getTypeName(json);
			return update(type, json);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String update(String type, String json)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException {
		try {
			UUID uuid = UUIDUtility.getUUID(json);
			return update(type, json, uuid);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String update(String type, String json, UUID uuid)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to create {} : {}", type, json);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.header(CONTENT_TYPE_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(InstancePath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());
			
			Map<String,String> parameters = new HashMap<>();
			addIncludeContexts(parameters);
			addIncludeMeta(parameters);
			addIncludeAllMeta(parameters);
			gxHTTPStringRequest.queryParams(parameters);

			HttpURLConnection httpURLConnection = gxHTTPStringRequest.put(json);
			String ret = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.trace("{} with UUID {} successfully created : {}", type, uuid, ret);
			return ret;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public <ERElem extends ERElement> boolean delete(ERElem er)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException {
		try {
			String type = er.getTypeName();
			UUID uuid = er.getID();
			return delete(type, uuid);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean delete(String type, UUID uuid)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to delete {} with UUID {}", type, uuid);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.header(ACCEPT_HTTP_HEADER_KEY, GXConnection.APPLICATION_JSON_CHARSET_UTF_8);
			gxHTTPStringRequest.path(InstancePath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(uuid.toString());

			HttpURLConnection httpURLConnection = gxHTTPStringRequest.delete();
			HTTPUtility.getResponse(String.class, httpURLConnection);

			boolean deleted = true;

			logger.info("{} with UUID {} {}", type, uuid, deleted ? " successfully deleted" : "was NOT deleted");
			return deleted;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public <F extends Facet> F createFacet(F facet)
			throws SchemaViolationException, FacetAlreadyPresentException, ResourceRegistryException {
		return create(facet);
	}

	@Override
	public String createFacet(String facet)
			throws SchemaViolationException, FacetAlreadyPresentException, ResourceRegistryException {
		return create(facet);
	}

	@Override
	public <F extends Facet> F readFacet(F facet) 
			throws FacetNotFoundException, FacetAvailableInAnotherContextException, ResourceRegistryException {
		return read(facet);
	}

	@Override
	public String readFacet(String facetType, UUID uuid) 
			throws FacetNotFoundException, FacetAvailableInAnotherContextException, ResourceRegistryException {
		return read(facetType, uuid);
	}
	
	@Override
	public <F extends Facet> F updateFacet(F facet)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException {
		return update(facet);
	}

	@Override
	public String updateFacet(String facet)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException {
		return update(facet);
	}

	@Override
	public <F extends Facet> boolean deleteFacet(F facet)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException {
		return delete(facet);
	}

	@Override
	public boolean deleteFacet(String facetType, UUID uuid)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException {
		return delete(facetType, uuid);
	}

	@Override
	public <R extends Resource> R createResource(R resource)
			throws SchemaViolationException, ResourceAlreadyPresentException, ResourceRegistryException {
		return create(resource);
	}

	@Override
	public String createResource(String resource)
			throws SchemaViolationException, ResourceAlreadyPresentException, ResourceRegistryException {
		return create(resource);
	}

	@Override
	public <R extends Resource> R readResource(R resource) 
			throws ResourceNotFoundException, ResourceAvailableInAnotherContextException, ResourceRegistryException {
		return read(resource);
	}

	@Override
	public String readResource(String resourceType, UUID uuid)
			throws ResourceNotFoundException, ResourceAvailableInAnotherContextException, ResourceRegistryException {
		return read(resourceType, uuid);
	}

	@Override
	public <R extends Resource> R updateResource(R resource)
			throws SchemaViolationException, ResourceNotFoundException, ResourceRegistryException {
		return update(resource);
	}

	@Override
	public String updateResource(String resource)
			throws SchemaViolationException, ResourceNotFoundException, ResourceRegistryException {
		return update(resource);
	}

	@Override
	public <R extends Resource> boolean deleteResource(R resource)
			throws ResourceNotFoundException, ResourceRegistryException {
		return delete(resource);
	}

	@Override
	public boolean deleteResource(String resourceType, UUID uuid)
			throws ResourceNotFoundException, ResourceRegistryException {
		return delete(resourceType, uuid);
	}

	@Override
	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> C createConsistsOf(C consistsOf)
			throws SchemaViolationException, ConsistsOfAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException {
		return create(consistsOf);
	}

	@Override
	public String createConsistsOf(String consistsOf)
			throws SchemaViolationException, ConsistsOfAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException {
		return create(consistsOf);
	}

	@Override
	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> C readConsistsOf(C consistsOf)
			throws ConsistsOfNotFoundException, ConsistsOfAvailableInAnotherContextException, ResourceRegistryException {
		return read(consistsOf);
	}

	@Override
	public String readConsistsOf(String consistsOfType, UUID uuid) 
			throws ConsistsOfNotFoundException, ConsistsOfAvailableInAnotherContextException, ResourceRegistryException {
		return read(consistsOfType, uuid);
	}

	@Override
	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> C updateConsistsOf(C consistsOf)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException {
		return update(consistsOf);
	}

	@Override
	public String updateConsistsOf(String consistsOf)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException {
		return update(consistsOf);
	}

	@Override
	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> boolean deleteConsistsOf(C consistsOf)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException {
		return delete(consistsOf);
	}

	@Override
	public boolean deleteConsistsOf(String consistsOfType, UUID uuid)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException {
		return delete(consistsOfType, uuid);
	}

	@Override
	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> I createIsRelatedTo(I isRelatedTo)
			throws SchemaViolationException, IsRelatedToAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException {
		return create(isRelatedTo);
	}

	@Override
	public String createIsRelatedTo(String isRelatedTo)
			throws SchemaViolationException, IsRelatedToAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException {
		return create(isRelatedTo);
	}

	@Override
	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> I readIsRelatedTo(I isRelatedTo)
			throws IsRelatedToNotFoundException, IsRelatedToAvailableInAnotherContextException, ResourceRegistryException {
		return read(isRelatedTo);
	}

	@Override
	public String readIsRelatedTo(String isRelatedToType, UUID uuid)
			throws IsRelatedToNotFoundException, IsRelatedToAvailableInAnotherContextException, ResourceRegistryException {
		return read(isRelatedToType, uuid);
	}

	@Override
	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> I updateIsRelatedTo(I isRelatedTo)
			throws SchemaViolationException, IsRelatedToNotFoundException, ResourceRegistryException {
		return update(isRelatedTo);
	}

	@Override
	public String updateIsRelatedTo(String isRelatedTo)
			throws SchemaViolationException, IsRelatedToNotFoundException, ResourceRegistryException {
		return update(isRelatedTo);
	}

	@Override
	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> boolean deleteIsRelatedTo(I isRelatedTo)
			throws IsRelatedToNotFoundException, ResourceRegistryException {
		return delete(isRelatedTo);
	}

	@Override
	public boolean deleteIsRelatedTo(String isRelatedToType, UUID uuid)
			throws IsRelatedToNotFoundException, ResourceRegistryException {
		return delete(isRelatedToType, uuid);
	}

	@Override
	public List<ERElement> addToContext(String type, UUID instanceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to add {} with UUID {} to {} with UUID {} ", type, instanceUUID, Context.NAME,
					contextUUID);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.path(SharingPath.SHARING_PATH_PART);
			gxHTTPStringRequest.path(SharingPath.CONTEXTS_PATH_PART);
			gxHTTPStringRequest.path(contextUUID.toString());
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(instanceUUID.toString());

			
			Map<String, String> queryParams = new HashMap<>();
			queryParams.put(SharingPath.OPERATION_QUERY_PARAMETER, SharingOperation.ADD.name());
			queryParams.put(SharingPath.DRY_RUN_QUERY_QUERY_PARAMETER, dryRun.toString());
			Boolean forceAddToContext = getCurrentContextUUID().compareTo(contextUUID)==0;
			queryParams.put(SharingPath.FORCE_ADD_TO_CONTEXT_QUERY_PARAMETER, forceAddToContext.toString());
			
			addIncludeMeta(queryParams);
			addIncludeAllMeta(queryParams);
			addIncludeContexts(queryParams);
			gxHTTPStringRequest.queryParams(queryParams);
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.post();
			String jsonArray = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.info("{} with UUID {} successfully added to {} with UUID {}", type, instanceUUID, Context.NAME, contextUUID);
			List<ERElement> affectedInstaces = ElementMapper.unmarshalList(jsonArray);
			
			return affectedInstaces;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ERElement> addToContext(ERElement er, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		try {
			String type = er.getTypeName();
			UUID instanceUUID = er.getID();
			return addToContext(type, instanceUUID, contextUUID, dryRun);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ERElement> addToCurrentContext(String type, UUID instanceUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		UUID contextUUID = getCurrentContextUUID();
		return addToContext(type, instanceUUID, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> addToCurrentContext(ERElement er, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		UUID contextUUID = getCurrentContextUUID();
		return addToContext(er, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> removeFromContext(String type, UUID instanceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to add {} with UUID {} to {} with UUID {} ", type, instanceUUID, Context.NAME,
					contextUUID);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.path(SharingPath.SHARING_PATH_PART);
			gxHTTPStringRequest.path(SharingPath.CONTEXTS_PATH_PART);
			gxHTTPStringRequest.path(contextUUID.toString());
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(instanceUUID.toString());
			
			Map<String, String> queryParams = new HashMap<>();
			queryParams.put(SharingPath.OPERATION_QUERY_PARAMETER, SharingOperation.REMOVE.name());
			queryParams.put(SharingPath.DRY_RUN_QUERY_QUERY_PARAMETER, dryRun.toString());
			
			addIncludeMeta(queryParams);
			addIncludeAllMeta(queryParams);
			addIncludeContexts(queryParams);
			gxHTTPStringRequest.queryParams(queryParams);
			
			
			HttpURLConnection httpURLConnection = gxHTTPStringRequest.post();
			String jsonArray = HTTPUtility.getResponse(String.class, httpURLConnection);

			logger.info("{} with UUID {} successfully removed from {} with UUID {}", type, instanceUUID, Context.NAME, contextUUID);
			List<ERElement> affectedInstaces = ElementMapper.unmarshalList(jsonArray);
			
			return affectedInstaces;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ERElement> removeFromContext(ERElement er, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		try {
			String type = er.getTypeName();
			UUID instanceUUID = er.getID();
			return removeFromContext(type, instanceUUID, contextUUID, dryRun);
		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ERElement> removeFromCurrentContext(String type, UUID instanceUUID, Boolean dryRun) 
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException {
		UUID contextUUID = getCurrentContextUUID();
		return removeFromContext(type, instanceUUID, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> removeFromCurrentContext(ERElement er, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException,
			ResourceRegistryException {
		UUID contextUUID = getCurrentContextUUID();
		return removeFromContext(er, contextUUID, dryRun);
	}

	@Override
	public Map<UUID, String> getElementContexts(String type, UUID instanceUUID)
			throws NotFoundException, ResourceRegistryException {
		try {
			logger.trace("Going to get contexts of {} with UUID {}", type, instanceUUID);
			GXHTTPStringRequest gxHTTPStringRequest = getGXHTTPStringRequest();
			gxHTTPStringRequest.path(InstancePath.INSTANCES_PATH_PART);
			gxHTTPStringRequest.path(type);
			gxHTTPStringRequest.path(instanceUUID.toString());
			gxHTTPStringRequest.path(SharingPath.CONTEXTS_PATH_PART);

			HttpURLConnection httpURLConnection = gxHTTPStringRequest.get();
			String objectNode = HTTPUtility.getResponse(String.class, httpURLConnection);
			
			logger.info("Contexts of {} with UUID {} are {}", type, instanceUUID, objectNode);
			
			Map<UUID, String> contexts = ContextUtility.getContextMap(objectNode);

			return contexts;

		} catch (ResourceRegistryException e) {
			// logger.trace("Error Creating {}", facet, e);
			throw e;
		} catch (Exception e) {
			// logger.trace("Error Creating {}", facet, e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public <ERElem extends ERElement> Map<UUID, String> getElementContexts(ERElem er)
			throws NotFoundException, ResourceRegistryException {
		String type = er.getTypeName();
		UUID instanceUUID = er.getID();
		return getElementContexts(type, instanceUUID);
	}

	@Override
	public List<ERElement> addResourceToContext(String resourceType, UUID resourceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToContext(resourceType, resourceUUID, contextUUID, dryRun);
	}

	@Override
	public <R extends Resource> List<ERElement> addResourceToContext(R resource, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToContext(resource, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> addResourceToCurrentContext(String resourceType, UUID resourceUUID, Boolean dryRun) 
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToCurrentContext(resourceType, resourceUUID, dryRun);
	}

	@Override
	public <R extends Resource> List<ERElement> addResourceToCurrentContext(R resource, Boolean dryRun) 
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToCurrentContext(resource, dryRun);
	}

	@Override
	public List<ERElement> removeResourceFromContext(String resourceType, UUID resourceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromContext(resourceType, resourceUUID, contextUUID, dryRun);
	}

	@Override
	public <R extends Resource> List<ERElement> removeResourceFromContext(R resource, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromContext(resource, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> removeResourceFromCurrentContext(String resourceType, UUID resourceUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromCurrentContext(resourceType, resourceUUID, dryRun);
	}

	@Override
	public <R extends Resource> List<ERElement> removeResourceFromCurrentContext(R resource, Boolean dryRun) throws SchemaViolationException,
			ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromCurrentContext(resource, dryRun);
	}

	@Override
	public Map<UUID, String> getResourceContexts(String resourceType, UUID resourceUUID)
			throws ResourceNotFoundException, ResourceRegistryException {
		return getElementContexts(resourceType, resourceUUID);
	}

	@Override
	public <R extends Resource> Map<UUID, String> getResourceContexts(R resource)
			throws ResourceNotFoundException, ResourceRegistryException {
		return getElementContexts(resource);
	}
	
	@Override
	public List<ERElement> addFacetToContext(String facetType, UUID facetUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToContext(facetType, facetUUID, contextUUID, dryRun);
	}

	@Override
	public <F extends Facet> List<ERElement> addFacetToContext(F facet, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToContext(facet, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> addFacetToCurrentContext(String facetType, UUID facetUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToCurrentContext(facetType, facetUUID, dryRun);
	}

	@Override
	public <F extends Facet> List<ERElement> addFacetToCurrentContext(F facet, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return addToCurrentContext(facet, dryRun);
	}

	@Override
	public List<ERElement> removeFacetFromContext(String facetType, UUID facetUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromContext(facetType, facetUUID, contextUUID, dryRun);
	}

	@Override
	public <F extends Facet> List<ERElement> removeFacetFromContext(F facet, UUID contextUUID, Boolean dryRun) 
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromContext(facet, contextUUID, dryRun);
	}

	@Override
	public List<ERElement> removeFacetFromCurrentContext(String facetType, UUID facetUUID, Boolean dryRun) 
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromCurrentContext(facetType, facetUUID, dryRun);
	}

	@Override
	public <F extends Facet> List<ERElement> removeFacetFromCurrentContext(F facet, Boolean dryRun) 
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException {
		return removeFromCurrentContext(facet, dryRun);
	}

	@Override
	public Map<UUID, String> getFacetContexts(String facetType, UUID facetUUID)
			throws FacetNotFoundException, ResourceRegistryException {
		return getElementContexts(facetType, facetUUID);
	}

	@Override
	public <F extends Facet> Map<UUID, String> getFacetContexts(F facet)
			throws FacetNotFoundException, ResourceRegistryException {
		return getElementContexts(facet);
	}

}
