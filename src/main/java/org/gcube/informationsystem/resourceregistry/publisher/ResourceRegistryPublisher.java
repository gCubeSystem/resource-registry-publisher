package org.gcube.informationsystem.resourceregistry.publisher;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.contexts.ContextNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.facet.FacetNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.consistsof.ConsistsOfAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.consistsof.ConsistsOfAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.consistsof.ConsistsOfNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToAlreadyPresentException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToAvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.relations.isrelatedto.IsRelatedToNotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.api.request.RequestInfo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface ResourceRegistryPublisher extends RequestInfo {
	
	/**
	 * Use {@link #includeContexts()} instead
	 * @return
	 */
	@Deprecated
	public boolean isIncludeContextsInHeader();

	/**
	 * Use {@link #includeContexts(boolean)} instead
	 * @param includeContextsInHeader
	 */
	@Deprecated
	public void setIncludeContextsInHeader(boolean includeContexts);
	
	public void addHeader(String name, String value);
	
	public ContextCache getContextCache();
	
	/**
	 * Use {@link #getContexts()} instead
	 * @return an array containing all contexts definition
	 * @throws ResourceRegistryException if fails
	 */
	@Deprecated
	public List<Context> getAllContext() throws ResourceRegistryException;
	
	/**
	 * @return an array containing all contexts definition
	 * @throws ResourceRegistryException if fails
	 */
	public List<Context> getContexts() throws ResourceRegistryException;
	
	public Context getContext(UUID uuid) throws ContextNotFoundException, ResourceRegistryException;

	public Context getCurrentContext() throws ContextNotFoundException, ResourceRegistryException;
	
	/* ----- */
		
	public <ERElem extends ERElement> List<ERElem> list(Class<ERElem> clazz, Boolean polymorphic)
			throws ResourceRegistryException;
	
	public String list(String type, Boolean polymorphic) 
			throws ResourceRegistryException;
	
	
	public <ERElem extends ERElement> ERElem create(ERElem er)
			throws SchemaViolationException, AlreadyPresentException, ResourceRegistryException;

	public String create(String json)
			throws SchemaViolationException, AlreadyPresentException, ResourceRegistryException;
	
	
	public <ERElem extends ERElement> boolean exist(ERElem er)
			throws AvailableInAnotherContextException, ResourceRegistryException;

	public <ERElem extends ERElement> boolean exist(Class<ERElem> clazz, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException;
	
	public boolean exist(String type, UUID uuid)
			throws AvailableInAnotherContextException, ResourceRegistryException;
	
	
	public <ERElem extends ERElement> ERElem read(ERElem er)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;

	public <ERElem extends ERElement> ERElem read(Class<ERElem> clazz, UUID uuid)
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;
	
	public String read(String type, UUID uuid) 
			throws NotFoundException, AvailableInAnotherContextException, ResourceRegistryException;
	
	
	public <ERElem extends ERElement> ERElem update(ERElem er)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException;

	public String update(String json) 
			throws SchemaViolationException, NotFoundException, ResourceRegistryException;
	
	public String update(String type, String json)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException;
	
	public String update(String type, String json, UUID uuid)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException;
	
	
	public <ERElem extends ERElement> boolean delete(ERElem er)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException;

	public boolean delete(String type, UUID uuid)
			throws SchemaViolationException, NotFoundException, ResourceRegistryException;

	
	/* ----- */

	public <F extends Facet> F createFacet(F facet)
			throws SchemaViolationException, FacetAlreadyPresentException, ResourceRegistryException;

	public String createFacet(String facet)
			throws SchemaViolationException, FacetAlreadyPresentException, ResourceRegistryException;

	public <F extends Facet> F readFacet(F facet) 
			throws FacetNotFoundException, FacetAvailableInAnotherContextException, ResourceRegistryException;

	public String readFacet(String facetType, UUID uuid) 
			throws FacetNotFoundException, FacetAvailableInAnotherContextException, ResourceRegistryException;

	public <F extends Facet> F updateFacet(F facet)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException;

	public String updateFacet(String facet)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException;

	public <F extends Facet> boolean deleteFacet(F facet)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException;

	public boolean deleteFacet(String facetType, UUID uuid)
			throws SchemaViolationException, FacetNotFoundException, ResourceRegistryException;

	/* ----- */

	public <R extends Resource> R createResource(R resource)
			throws SchemaViolationException, ResourceAlreadyPresentException, ResourceRegistryException;

	public String createResource(String resource)
			throws SchemaViolationException, ResourceAlreadyPresentException, ResourceRegistryException;

	public <R extends Resource> R readResource(R resource) 
			throws ResourceNotFoundException, ResourceAvailableInAnotherContextException, ResourceRegistryException;

	public String readResource(String resourceType, UUID uuid)
			throws ResourceNotFoundException, ResourceAvailableInAnotherContextException, ResourceRegistryException;

	public <R extends Resource> R updateResource(R resource)
			throws SchemaViolationException, ResourceNotFoundException, ResourceRegistryException;

	public String updateResource(String resource)
			throws SchemaViolationException, ResourceNotFoundException, ResourceRegistryException;

	public <R extends Resource> boolean deleteResource(R resource)
			throws ResourceNotFoundException, ResourceRegistryException;

	public boolean deleteResource(String resourceType, UUID uuid)
			throws ResourceNotFoundException, ResourceRegistryException;

	/* ----- */

	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> C createConsistsOf(C consistsOf)
			throws SchemaViolationException, ConsistsOfAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException;

	public String createConsistsOf(String consistsOf)
			throws SchemaViolationException, ConsistsOfAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException;

	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> C readConsistsOf(C consistsOf)
			throws ConsistsOfNotFoundException, ConsistsOfAvailableInAnotherContextException, ResourceRegistryException;

	public String readConsistsOf(String consistsOfType, UUID uuid) 
			throws ConsistsOfNotFoundException, ConsistsOfAvailableInAnotherContextException, ResourceRegistryException;

	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> C updateConsistsOf(C consistsOf)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException;

	public String updateConsistsOf(String consistsOf)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException;

	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> boolean deleteConsistsOf(C consistsOf)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException;

	public boolean deleteConsistsOf(String consistsOfType, UUID uuid)
			throws SchemaViolationException, ConsistsOfNotFoundException, ResourceRegistryException;

	/* ----- */

	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> I createIsRelatedTo(I isRelatedTo)
			throws SchemaViolationException, IsRelatedToAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException;

	public String createIsRelatedTo(String isRelatedTo)
			throws SchemaViolationException, IsRelatedToAlreadyPresentException, ResourceNotFoundException, ResourceRegistryException;

	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> I readIsRelatedTo(I isRelatedTo)
			throws IsRelatedToNotFoundException, IsRelatedToAvailableInAnotherContextException, ResourceRegistryException;

	public String readIsRelatedTo(String isRelatedToType, UUID uuid)
			throws IsRelatedToNotFoundException, IsRelatedToAvailableInAnotherContextException, ResourceRegistryException;

	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> I updateIsRelatedTo(I isRelatedTo)
			throws SchemaViolationException, IsRelatedToNotFoundException, ResourceRegistryException;

	public String updateIsRelatedTo(String isRelatedTo)
			throws SchemaViolationException, IsRelatedToNotFoundException, ResourceRegistryException;

	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> boolean deleteIsRelatedTo(I isRelatedTo)
			throws IsRelatedToNotFoundException, ResourceRegistryException;

	public boolean deleteIsRelatedTo(String isRelatedToType, UUID uuid)
			throws IsRelatedToNotFoundException, ResourceRegistryException;

	/* ----- */

	public List<ERElement> addToContext(String type, UUID instanceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> addToContext(ERElement er, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> addToCurrentContext(String type, UUID instanceUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> addToCurrentContext(ERElement er, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeFromContext(String type, UUID instanceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeFromContext(ERElement er, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeFromCurrentContext(String type, UUID instanceUUID, Boolean dryRun) 
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeFromCurrentContext(ERElement er, Boolean dryRun)
			throws SchemaViolationException, NotFoundException, ContextNotFoundException, ResourceRegistryException;
	
	public Map<UUID, String> getElementContexts(String type, UUID instanceUUID)
			throws NotFoundException, ResourceRegistryException;

	public <ERElem extends ERElement> Map<UUID, String> getElementContexts(ERElem er)
			throws NotFoundException, ResourceRegistryException;

	/* ----- */

	public List<ERElement> addResourceToContext(String resourceType, UUID resourceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <R extends Resource> List<ERElement> addResourceToContext(R resource, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> addResourceToCurrentContext(String resourceType, UUID resourceUUID, Boolean dryRun) 
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <R extends Resource> List<ERElement> addResourceToCurrentContext(R resource, Boolean dryRun) 
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeResourceFromContext(String resourceType, UUID resourceUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <R extends Resource> List<ERElement> removeResourceFromContext(R resource, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeResourceFromCurrentContext(String resourceType, UUID resourceUUID, Boolean dryRun)
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <R extends Resource> List<ERElement> removeResourceFromCurrentContext(R resource, Boolean dryRun) 
			throws SchemaViolationException, ResourceNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public Map<UUID, String> getResourceContexts(String resourceType, UUID resourceUUID)
			throws ResourceNotFoundException, ResourceRegistryException;

	public <R extends Resource> Map<UUID, String> getResourceContexts(R resource)
			throws ResourceNotFoundException, ResourceRegistryException;

	/* ----- */

	public List<ERElement> addFacetToContext(String facetType, UUID facetUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <F extends Facet> List<ERElement> addFacetToContext(F facet, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> addFacetToCurrentContext(String facetType, UUID facetUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <F extends Facet> List<ERElement> addFacetToCurrentContext(F facet, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeFacetFromContext(String facetType, UUID facetUUID, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <F extends Facet> List<ERElement> removeFacetFromContext(F facet, UUID contextUUID, Boolean dryRun)
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public List<ERElement> removeFacetFromCurrentContext(String facetType, UUID facetUUID, Boolean dryRun) 
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public <F extends Facet> List<ERElement> removeFacetFromCurrentContext(F facet, Boolean dryRun) 
			throws SchemaViolationException, FacetNotFoundException, ContextNotFoundException, ResourceRegistryException;

	public Map<UUID, String> getFacetContexts(String facetType, UUID facetUUID)
			throws FacetNotFoundException, ResourceRegistryException;

	public <F extends Facet> Map<UUID, String> getFacetContexts(F facet)
			throws FacetNotFoundException, ResourceRegistryException;

}
