package org.gcube.informationsystem.resourceregistry.publisher;

import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.UUIDManager;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SimpleFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.RunningPluginImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SimpleFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.entities.resources.RunningPlugin;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InvalidOperationTest extends ERManagementTest {
	
	private static Logger logger = LoggerFactory.getLogger(InvalidOperationTest.class);
	
	public static final String ACTIVATES = "{\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"add\":\"propagate\",\"remove\":\"cascade\",\"delete\":\"cascade\"},\"" + Element.TYPE_PROPERTY + "\":\"Activates\",\"source\":{\"" + Element.TYPE_PROPERTY + "\":\"Configuration\",\"" + IdentifiableElement.ID_PROPERTY + "\":\"CONFIGURATION_UUID\"},\"target\":{\"" + IdentifiableElement.ID_PROPERTY + "\":\"ESERVICE_UUID\",\"" + Element.TYPE_PROPERTY + "\":\"EService\"}}";
	public static final String ACTOR = "{\"" + Element.TYPE_PROPERTY + "\":\"Actor\",\"metadata\":null,\"consistsOf\":[{\"" + Element.TYPE_PROPERTY + "\":\"IsIdentifiedBy\",\"metadata\":null,\"propagationConstraint\":{\"" + Element.TYPE_PROPERTY + "\":\"PropagationConstraint\",\"remove\":\"cascade\",\"delete\":\"cascade\",\"add\":\"propagate\"},\"source\":{\"" + Element.TYPE_PROPERTY + "\":\"Actor\",\"metadata\":null},\"target\":{\"" + Element.TYPE_PROPERTY + "\":\"ContactFacet\",\"metadata\":null,\"title\":\"Dr.\",\"name\":\"Frosini\",\"middleName\":null,\"surname\":null,\"eMail\":\"luca.frosini@isti.cnr.it\"}}],\"isRelatedTo\":[]}";
	
	@Test(expected = SchemaViolationException.class)
	public void createInvalidIsRealtedTo() throws Exception {
		Configuration configuration = createConfiguration();
		EService eService = createEService();

		try {
			/*
			 * Trying to create a relation activates between a Configuration and EService
			 * The creation MUST fails raising SchemaViolationException because the
			 * Activates relation is between two Service isntaces
			 *
			 * The only way to try to create it is using static string because Java classes
			 * already deny to create and instance of Activates
			 * 
			 * Here we want to test how the service behave if a client does not behave properly
			 */
			
			String json = ACTIVATES.replace("CONFIGURATION_UUID", configuration.getID().toString());
			json = json.replace("ESERVICE_UUID", eService.getID().toString());
			
			((ResourceRegistryPublisherImpl)resourceRegistryPublisher).create(Activates.NAME, json, UUIDManager.getInstance().generateValidUUID());
			
		}finally {
			resourceRegistryPublisher.delete(configuration);
			resourceRegistryPublisher.delete(eService);
		}
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateStandAloneFacet() throws Exception {
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");

		resourceRegistryPublisher.create(cpuFacet);
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateInvalidRunningPlugin() throws Exception {
		RunningPlugin runningPlugin = new RunningPluginImpl();

		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("information-system");
		softwareFacet.setName("is-exporter-se-plugin");
		softwareFacet.setVersion("1.0.0");
		
		IsIdentifiedBy<RunningPlugin, SoftwareFacet> isIdentifiedBy = new IsIdentifiedByImpl<>(runningPlugin, softwareFacet);
		runningPlugin.addFacet(isIdentifiedBy);

		resourceRegistryPublisher.create(runningPlugin);
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateAbstractEntity() throws Exception {
		((ResourceRegistryPublisherImpl)resourceRegistryPublisher).create(Actor.NAME, ACTOR, UUIDManager.getInstance().generateValidUUID());
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateHostingNodeAndEServiceWithSharedFacet() throws Exception {
		ERManagementTest erManagementTest = new ERManagementTest();
		Map<String, Resource> map = erManagementTest.createHostingNodeAndEService();
		EService eService = (EService) map.get(EService.NAME);
		HostingNode hostingNode = (HostingNode) map.get(HostingNode.NAME);
		try {
			Facet shared = hostingNode.getConsistsOf().get(0).getTarget();
			ConsistsOf<EService, Facet> consistsOf = new ConsistsOfImpl<>(eService, shared);
			consistsOf = resourceRegistryPublisher.create(consistsOf);
			
			logger.debug("Created : {}", consistsOf);
			
		} finally {
			resourceRegistryPublisher.delete(eService);
			resourceRegistryPublisher.delete(hostingNode);
		}

	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateEServiceAndDeleteRequiredConsistsOf() throws Exception {
		EService eService = null;
		try {
			ERManagementTest erManagementTest = new ERManagementTest();
			eService = erManagementTest.createEService();
			
			@SuppressWarnings("unchecked")
			IsIdentifiedBy<EService, SoftwareFacet> isIdentifiedBy = (IsIdentifiedBy<EService, SoftwareFacet>) eService.getConsistsOf(IsIdentifiedBy.class).get(0);
			resourceRegistryPublisher.delete(isIdentifiedBy);
		}finally {
			resourceRegistryPublisher.delete(eService);
		}
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateEServiceAndDeleteRequiredFacet() throws Exception {
		ERManagementTest erManagementTest = new ERManagementTest();
		EService eService = erManagementTest.createEService();
		
		@SuppressWarnings("unchecked")
		IsIdentifiedBy<EService, SoftwareFacet> isIdentifiedBy = (IsIdentifiedBy<EService, SoftwareFacet>) eService.getConsistsOf(IsIdentifiedBy.class).get(0);
		SoftwareFacet softwareFacet = isIdentifiedBy.getTarget();
		try {
			resourceRegistryPublisher.delete(softwareFacet);
		}finally {
			resourceRegistryPublisher.delete(eService);
		}
	
	}
	
	@Test(expected = SchemaViolationException.class)
	public void testCreateConsistsOfBeetweenResources() throws Exception {
		ERManagementTest erManagementTest = new ERManagementTest();
		Map<String, Resource> map = erManagementTest.createHostingNodeAndEService();

		UUID hostingNodeUUID = map.get(HostingNode.NAME).getID();
		UUID eServiceUUID = map.get(EService.NAME).getID();

		HostingNode hostingNode = new HostingNodeImpl();
		hostingNode.setID(hostingNodeUUID);

		SimpleFacet fakeEServiceAsSimpleFacet = new SimpleFacetImpl();
		fakeEServiceAsSimpleFacet.setID(eServiceUUID);

		ConsistsOf<HostingNode, SimpleFacet> consistsOf = new ConsistsOfImpl<HostingNode, SimpleFacet>(hostingNode, fakeEServiceAsSimpleFacet);

		try {
			String json = ElementMapper.marshal(consistsOf);
			json = json.replaceAll(SimpleFacet.NAME, EService.NAME);
			
			((ResourceRegistryPublisherImpl)resourceRegistryPublisher).create(ConsistsOf.NAME, json, UUIDManager.getInstance().generateValidUUID());
			throw new Exception("A ConsistsOf has been created between two resoures. This should not happen");
		} finally {
			resourceRegistryPublisher.delete(map.get(EService.NAME));
			resourceRegistryPublisher.delete(map.get(HostingNode.NAME));
		}

	}
}
