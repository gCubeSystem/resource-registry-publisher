/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.publisher;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * To properly use thois test you must comment 
 * gcube-model dependency
 * @author Luca Frosini (ISTI - CNR)
 */
public class NoGcubeModelTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(NoGcubeModelTest.class);
	
	public static final String GROUP = "InformationSystem";
	public static final String NAME = "resource-registry";
	public static final String VERSION = "1.0.0";
	public static final String NEW_VERSION = "2.0.0";
	
	protected ResourceRegistryPublisher resourceRegistryPublisher;
	
	public NoGcubeModelTest() {
		Object rrURLOBj = ContextTest.properties.get(RESOURCE_REGISTRY_URL_PROPERTY);
		if(rrURLOBj!=null  && !rrURLOBj.toString().isEmpty()) {
			resourceRegistryPublisher = new ResourceRegistryPublisherImpl(rrURLOBj.toString());
		}else {
			resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
		}
		resourceRegistryPublisher.setIncludeMeta(true);
		resourceRegistryPublisher.setAllMeta(true);
	}
	
	@Ignore
	@Test
	public void testCreateResourceFromJson() throws Exception {
		String json = "{\"type\":\"VirtualService\",\"consistsOf\":[{\"type\":\"IsIdentifiedBy\",\"target\":{\"name\":\"aaaaaa\",\"optional\":\"true\",\"group\":\"xxxxx\",\"description\":\"\",\"version\":\"cvcvvv\",\"qualifier\":\"\",\"type\":\"SoftwareFacet\",\"aaaa\":\"dddddd\"}}]}";
		String ret = resourceRegistryPublisher.createResource(json);
		logger.info(ret);
	}

}
