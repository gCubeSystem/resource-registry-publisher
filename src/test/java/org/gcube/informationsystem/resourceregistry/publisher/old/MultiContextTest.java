/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.publisher.old;

import java.net.URI;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.resourceregistry.api.contexts.ContextCache;
import org.gcube.informationsystem.resourceregistry.api.exceptions.AvailableInAnotherContextException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.NotFoundException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientImpl;
import org.gcube.informationsystem.resourceregistry.publisher.ContextTest;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisher;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisherFactory;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisherImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.EventFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.LicenseFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.MemoryFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.NetworkingFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.StateFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.EServiceImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasPersistentMemoryImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasVolatileMemoryImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.LicenseFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet.MemoryUnit;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class MultiContextTest extends ContextTest {

	private static Logger logger = LoggerFactory
			.getLogger(MultiContextTest.class);

	protected ResourceRegistryPublisher resourceRegistryPublisher;
	protected ResourceRegistryClient resourceRegistryClient;
	
	public MultiContextTest() throws Exception {
		Object rrURLOBj = ContextTest.properties.get(RESOURCE_REGISTRY_URL_PROPERTY);
		if(rrURLOBj!=null  && !rrURLOBj.toString().isEmpty()) {
			resourceRegistryPublisher = new ResourceRegistryPublisherImpl(rrURLOBj.toString());
			resourceRegistryClient = new ResourceRegistryClientImpl(rrURLOBj.toString());
		}else {
			resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
			resourceRegistryClient = ResourceRegistryClientFactory.create();
		}
	}
	
	@Test
	public void testCreateEServiceHostingNode() throws Exception {
		EService eService = new EServiceImpl();
		
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		IsIdentifiedBy<EService, Facet> isIdentifiedBy = new IsIdentifiedByImpl<EService, Facet>(eService, softwareFacet, null);
		eService.addFacet(isIdentifiedBy);
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		LicenseFacet licenseFacet = new LicenseFacetImpl();
		licenseFacet.setName("EUPL");
		licenseFacet.setTextURL(new URL("https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11"));
		eService.addFacet(licenseFacet);
		
		EService createdEService = resourceRegistryPublisher.createResource(eService);
		logger.debug("Created : {}", createdEService);
		
		HostingNode hostingNode = new HostingNodeImpl();
				
		NetworkingFacet networkingFacet = new NetworkingFacetImpl();
		networkingFacet.setIPAddress("146.48.87.183");
		networkingFacet.setHostName("pc-frosini.isti.cnr.it");
		networkingFacet.setDomainName("isti.cnr.it");
		networkingFacet.setMask("255.255.248.0");
		networkingFacet.setBroadcastAddress("146.48.87.255");
		
		IsIdentifiedBy<HostingNode, NetworkingFacet> isIdentifiedByHNNF = new IsIdentifiedByImpl<HostingNode, NetworkingFacet>(hostingNode, networkingFacet, null);
		hostingNode.addFacet(isIdentifiedByHNNF);
		
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		hostingNode.addFacet(cpuFacet);
		
		
		MemoryFacet ram = new MemoryFacetImpl();
		ram.setSize(8);
		ram.setUnit(MemoryUnit.GB);
		ram.setUsed(2);
		
		HasVolatileMemory<HostingNode, MemoryFacet> hasVolatileMemory = 
				new HasVolatileMemoryImpl<HostingNode, MemoryFacet>(hostingNode, ram, null);
		hostingNode.addFacet(hasVolatileMemory);
		
		
		MemoryFacet disk = new MemoryFacetImpl();
		disk.setSize(256);
		disk.setUnit(MemoryUnit.GB);
		disk.setUsed(120);
		
		HasPersistentMemory<HostingNode, MemoryFacet> hasPersistentMemory = 
				new HasPersistentMemoryImpl<HostingNode, MemoryFacet>(hostingNode, disk, null);
		hostingNode.addFacet(hasPersistentMemory);
		
		StateFacet hnStateFacet = new StateFacetImpl();
		hnStateFacet.setValue("certified");
		hostingNode.addFacet(hnStateFacet);

		EventFacet hnEventFacet = new EventFacetImpl();
		hnEventFacet.setDate(Calendar.getInstance().getTime());
		hnEventFacet.setEvent("Created");
		hostingNode.addFacet(hnEventFacet);
		
		
		Activates<HostingNode, EService> activates = new ActivatesImpl<HostingNode, EService>(hostingNode, createdEService, null);
		hostingNode.attachResource(activates);
		
		HostingNode createdHN = resourceRegistryPublisher.createResource(hostingNode);
		logger.debug("Created : {}", createdHN);
		
		UUID hostingNodeUUID = createdHN.getID();
		UUID eServiceUUID = createdEService.getID();
		
		UUID alternativeContextUUID = ContextCache.getInstance().getUUIDByFullName(ALTERNATIVE_TEST_SCOPE);
		
		resourceRegistryPublisher.addResourceToContext(createdHN, alternativeContextUUID, false);
		
		logger.debug("Changing token to test add to scope");
		ContextTest.setContextByName(ContextTest.ALTERNATIVE_TEST_SCOPE);
		
		try {
			resourceRegistryClient.existInstance(EService.class, eServiceUUID);
		} catch (NotFoundException e) {
			throw e;
		} catch (AvailableInAnotherContextException e) {
			// Good
		} catch (ResourceRegistryException e) {
			throw e;
		}
		
		
		try {
			resourceRegistryClient.existInstance(HostingNode.class, hostingNodeUUID);
		} catch (NotFoundException e) {
			throw e;
		} catch (AvailableInAnotherContextException e) {
			// Good
		} catch (ResourceRegistryException e) {
			throw e;
		}
		
		
		List<ERElement> affectedInstances = resourceRegistryPublisher.addResourceToCurrentContext(createdHN, false);
		logger.debug("{} added to context. Affected instances are {}", createdHN, affectedInstances);
		//Assert.assertTrue(addedToContext);
		
		try {
			resourceRegistryClient.existInstance(EService.class, eServiceUUID);
		}catch(AvailableInAnotherContextException e){
			logger.debug("Resource with {} Available in another Context as Expected", eServiceUUID.toString());
		}
		
		boolean deleted = resourceRegistryPublisher.deleteResource(createdHN);
		Assert.assertTrue(deleted);
		
		
		logger.debug("Restoring original scope");
		ContextTest.setContextByName(ContextTest.DEFAULT_TEST_SCOPE);
		
		deleted = resourceRegistryPublisher.deleteResource(createdEService);
		Assert.assertTrue(deleted);
		
	}
	
	@Test
	public void testGetContexts() throws Exception {
		List<Context> contexts = resourceRegistryPublisher.getContexts();
		logger.debug("{}", contexts);
		
		ContextCache contextCache = ContextCache.getInstance();
		Map<UUID, String> uuidToContextFullName = contextCache.getUUIDToContextFullNameAssociation();
		logger.debug("{}", uuidToContextFullName);

		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			if(c.getParent()!=null) {
				IsParentOf isParentOf = c.getParent();
				Context parentContext = isParentOf.getSource();
				UUID parentUUID = parentContext.getID();
				Assert.assertEquals(parentContext, contextCache.getContextByUUID(parentUUID));
				List<IsParentOf> children = parentContext.getChildren();
				boolean found = false;
				for(IsParentOf ipo : children) {
					if(ipo.equals(isParentOf)) {
						found = true;
						break;
					}
				}
				Assert.assertTrue(found);
				logger.debug("{} : {} (parent {} : {})", c.getID(), contextCache.getContextFullNameByUUID(uuid), parentUUID, contextCache.getContextFullNameByUUID(parentUUID));
			}else {
				logger.debug("{} : {}", c.getID(), contextCache.getContextFullNameByUUID(uuid));
			}
		}
		
		Context currentContext = resourceRegistryPublisher.getCurrentContext();
		logger.debug("Current context : {}", currentContext);
		
		for(Context c : contexts) {
			UUID uuid = c.getID();
			Context context = resourceRegistryPublisher.getContext(uuid);
			String fullName = ContextCache.getInstance().getContextFullNameByUUID(uuid);
			logger.debug("{} - {} : {}", uuid, fullName, context);
		}
	}
	
}
