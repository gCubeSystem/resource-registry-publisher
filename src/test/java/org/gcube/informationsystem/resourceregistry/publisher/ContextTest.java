/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.publisher;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.authorization.utils.secret.SecretUtility;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.KeycloakClientHelper;
import org.gcube.common.keycloak.model.TokenResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	protected static final String CONFIG_INI_FILENAME = "config.ini";
	
	public static final String PARENT_DEFAULT_TEST_SCOPE;
	public static final String DEFAULT_TEST_SCOPE;
	public static final String ALTERNATIVE_TEST_SCOPE;
	
	public static final String GCUBE;
	public static final String DEVNEXT;
	public static final String NEXTNEXT;
	public static final String DEVSEC;
	public static final String DEVVRE;
	
	protected static final Properties properties;
	
	public static final String TYPE_PROPERTY_KEY = "type";
	public static final String USERNAME_PROPERTY_KEY = "username"; 
	public static final String PASSWORD_PROPERTY_KEY = "password"; 
	public static final String CLIENT_ID_PROPERTY_KEY = "clientId"; 
	
	public static final String RESOURCE_REGISTRY_URL_PROPERTY = "RESOURCE_REGISTRY_URL";
	
	static {
		GCUBE = "/gcube";
		DEVNEXT = GCUBE + "/devNext";
		NEXTNEXT = DEVNEXT + "/NextNext";
		DEVSEC = GCUBE + "/devsec";
		DEVVRE = DEVSEC + "/devVRE";
		
		PARENT_DEFAULT_TEST_SCOPE = GCUBE;
		DEFAULT_TEST_SCOPE = DEVNEXT;
		ALTERNATIVE_TEST_SCOPE = NEXTNEXT;
		
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		try {
			// load the properties file
			properties.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	private enum Type{
		USER, CLIENT_ID
	};
	
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.instance.reset();
		SecretManager secretManager = new SecretManager();
		secretManager.addSecret(secret);
		SecretManagerProvider.instance.set(secretManager);
		SecretManagerProvider.instance.get().set();
	}
	
	public static void setContextByName(String fullContextName) throws Exception {
		logger.debug("Going to set credentials for context {}", fullContextName);
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}
	
	
	private static TokenResponse getJWTAccessToken(String context) throws Exception {
		Type type = Type.valueOf(properties.get(TYPE_PROPERTY_KEY).toString());
		
		TokenResponse tr = null;
		
		int index = context.indexOf('/', 1);
		String root = context.substring(0, index == -1 ? context.length() : index);
		
		switch (type) {
			case CLIENT_ID:
				String clientId = properties.getProperty(CLIENT_ID_PROPERTY_KEY);
				String clientSecret = properties.getProperty(root);
				
				tr = KeycloakClientFactory.newInstance().queryUMAToken(context, clientId, clientSecret, context, null);
				break;
		
			case USER:
			default:
				String username = properties.getProperty(USERNAME_PROPERTY_KEY);
				String password = properties.getProperty(PASSWORD_PROPERTY_KEY);
				
				switch (root) {
					case "/gcube":
					default:
						clientId = "next.d4science.org";
						break;
					
					case "/pred4s":
						clientId = "pre.d4science.org";
						break;
					
					case "/d4science.research-infrastructures.eu":
						clientId = "services.d4science.org";
						break;
				}
				clientSecret = null;
				
				tr = KeycloakClientHelper.getTokenForUser(context, username, password);
				break;
				
		}

		return tr;
		
	}
	
	public static Secret getSecretByContextName(String context) throws Exception {
		TokenResponse tr = getJWTAccessToken(context);
		Secret secret = new JWTSecret(tr.getAccessToken());
		return secret;
	}
	
	public static void setContext(String token) throws Exception {
		Secret secret = getSecret(token);
		set(secret);
	}
	
	private static Secret getSecret(String token) throws Exception {
		Secret secret = SecretUtility.getSecretByTokenString(token);
		return secret;
	}
	
	public static String getUser() {
		String user = "UNKNOWN";
		try {
			user = SecretManagerProvider.instance.get().getUser().getUsername();
		} catch(Exception e) {
			logger.error("Unable to retrieve user. {} will be used", user);
		}
		return user;
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(DEFAULT_TEST_SCOPE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecretManagerProvider.instance.reset();
	}
	
}
