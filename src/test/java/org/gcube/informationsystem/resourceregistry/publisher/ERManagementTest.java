/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.publisher;

import java.net.URI;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.impl.properties.EncryptedImpl;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.resourceregistry.api.exceptions.entities.resource.ResourceNotFoundException;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.EventFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.IdentifierFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.LicenseFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.MemoryFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.NetworkingFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.StateFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.ConfigurationImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.EServiceImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.properties.ValueSchemaImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasPersistentMemoryImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasVolatileMemoryImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.IdentifierFacet.IdentificationType;
import org.gcube.resourcemanagement.model.reference.entities.facets.LicenseFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet.MemoryUnit;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Configuration;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.properties.ValueSchema;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ERManagementTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(ERManagementTest.class);
	
	public static final String GROUP = "InformationSystem";
	public static final String NAME = "resource-registry";
	public static final String VERSION = "1.0.0";
	public static final String NEW_VERSION = "2.0.0";
	
	protected ResourceRegistryPublisher resourceRegistryPublisher;
	
	public ERManagementTest() {
		Object rrURLOBj = ContextTest.properties.get(RESOURCE_REGISTRY_URL_PROPERTY);
		if(rrURLOBj!=null  && !rrURLOBj.toString().isEmpty()) {
			resourceRegistryPublisher = new ResourceRegistryPublisherImpl(rrURLOBj.toString());
		}else {
			resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
		}
		resourceRegistryPublisher.setIncludeMeta(true);
		resourceRegistryPublisher.setAllMeta(true);
	}
	
	
	@Before
	@After
	public void cleanInstances() throws Exception {
		// Clean the environment first to avoid error if a previous tests fails without cleaning the env
		List<Resource> allResources = resourceRegistryPublisher.list(Resource.class, true);
		for(Resource r : allResources) {
			try {
				deleteResource(r);
			}catch (ResourceNotFoundException e) {
				// A resource could be already deleted deleting another resource giving the propagation constraint 
			}
		}
	}
	
	
	public static SoftwareFacet getSoftwareFacet() {
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup(GROUP);
		softwareFacet.setName(NAME);
		softwareFacet.setVersion(VERSION);
		return softwareFacet;
	}
	
	public static void checkSoftwareFacetAssertion(SoftwareFacet softwareFacet, SoftwareFacet gotSoftwareFacet) {
		Assert.assertTrue(gotSoftwareFacet.getGroup().compareTo(softwareFacet.getGroup()) == 0);
		Assert.assertTrue(gotSoftwareFacet.getName().compareTo(softwareFacet.getName()) == 0);
		Assert.assertTrue(gotSoftwareFacet.getVersion().compareTo(softwareFacet.getVersion()) == 0);
	}
	
	public static Configuration instantiateValidConfiguration() throws Exception {
		Configuration configuration = new ConfigurationImpl();

		IdentifierFacet identifierFacet = new IdentifierFacetImpl();
		identifierFacet.setIdentificationType(IdentificationType.STRING);
		identifierFacet.setValue("MyID");
		identifierFacet.setPersistent(false);

		IsIdentifiedBy<Configuration, IdentifierFacet> isIdentifiedBy = new IsIdentifiedByImpl<Configuration, IdentifierFacet>(
				configuration, identifierFacet);
		configuration.addFacet(isIdentifiedBy);
		
		return configuration;
	}
	
	public static EService instantiateValidEService() throws Exception {
		EService eService = new EServiceImpl();

		SoftwareFacet softwareFacet = getSoftwareFacet();

		IsIdentifiedBy<Resource, Facet> isIdentifiedBy = new IsIdentifiedByImpl<Resource, Facet>(eService,
				softwareFacet, null);
		eService.addFacet(isIdentifiedBy);

		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);

		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);

		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);

		LicenseFacet licenseFacet = new LicenseFacetImpl();
		licenseFacet.setName("EUPL");
		licenseFacet.setTextURL(
				new URL("https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11"));
		eService.addFacet(licenseFacet);

		return eService;
	}

	public static HostingNode instantiateValidHostingNode() throws Exception {
		HostingNode hostingNode = new HostingNodeImpl();

		NetworkingFacet networkingFacet = new NetworkingFacetImpl();
		networkingFacet.setIPAddress("146.48.87.183");
		networkingFacet.setHostName("pc-frosini.isti.cnr.it");
		networkingFacet.setDomainName("isti.cnr.it");
		networkingFacet.setMask("255.255.248.0");
		networkingFacet.setBroadcastAddress("146.48.87.255");

		IsIdentifiedBy<HostingNode, NetworkingFacet> isIdentifiedBy = new IsIdentifiedByImpl<HostingNode, NetworkingFacet>(
				hostingNode, networkingFacet);
		hostingNode.addFacet(isIdentifiedBy);

		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		hostingNode.addFacet(cpuFacet);

		MemoryFacet persistentMemoryFacet = new MemoryFacetImpl();
		persistentMemoryFacet.setSize(1024);
		persistentMemoryFacet.setUnit(MemoryUnit.GB);
		persistentMemoryFacet.setUsed(512);
		HasPersistentMemory<HostingNode, MemoryFacet> hasPersistentMemory = new HasPersistentMemoryImpl<HostingNode, MemoryFacet>(
				hostingNode, persistentMemoryFacet);
		hostingNode.addFacet(hasPersistentMemory);

		MemoryFacet volatileMemoryFacet = new MemoryFacetImpl();
		volatileMemoryFacet.setSize(8);
		volatileMemoryFacet.setUnit(MemoryUnit.GB);
		volatileMemoryFacet.setUsed(4);
		HasVolatileMemory<HostingNode, MemoryFacet> hasVolatileMemory = new HasVolatileMemoryImpl<HostingNode, MemoryFacet>(
				hostingNode, volatileMemoryFacet);
		hostingNode.addFacet(hasVolatileMemory);

		StateFacet hnStateFacet = new StateFacetImpl();
		hnStateFacet.setValue("certified");
		hostingNode.addFacet(hnStateFacet);

		EventFacet hnEventFacet = new EventFacetImpl();
		hnEventFacet.setDate(Calendar.getInstance().getTime());
		hnEventFacet.setEvent("Created");
		hostingNode.addFacet(hnEventFacet);

		return hostingNode;
	}
	
	public static void checkUUIDAndMetadata(IdentifiableElement identifiableElement, IdentifiableElement createdIdentifiableElement) {
		UUID createdUUID = createdIdentifiableElement.getID();
		Assert.assertTrue(createdUUID!=null);
		
		if(identifiableElement.getID()!=null) {
			Assert.assertTrue(createdUUID.compareTo(identifiableElement.getID())==0);
		}
		
		Metadata createdMetadata = createdIdentifiableElement.getMetadata();
		Assert.assertTrue(createdMetadata!=null);
		
		String createdBy = createdMetadata.getCreatedBy();
		Assert.assertTrue(createdBy!=null);
		
		String lastUpdateBy = createdMetadata.getLastUpdateBy();
		Assert.assertTrue(lastUpdateBy!=null);
		
		Date creationTime = createdMetadata.getCreationTime();
		Assert.assertTrue(creationTime!=null);
		
		Date lastUpdateTime = createdMetadata.getLastUpdateTime();
		Assert.assertTrue(lastUpdateTime!=null);
		Assert.assertTrue(lastUpdateTime.equals(creationTime) || lastUpdateTime.equals(lastUpdateTime));
		
		Metadata metadata = identifiableElement.getMetadata();
		if(metadata!=null) { 
			if(metadata.getCreatedBy()!=null) {
				Assert.assertTrue(createdBy.compareTo(metadata.getCreatedBy())==0);
			}else {
				Assert.assertTrue(createdBy.compareTo(ContextTest.getUser())==0);
			}
			
			if(metadata.getLastUpdateBy()!=null) {
				Assert.assertTrue(lastUpdateBy.compareTo(metadata.getLastUpdateBy())==0);
			}else {
				Assert.assertTrue(lastUpdateBy.compareTo(ContextTest.getUser())==0);
			}
			
			if(metadata.getLastUpdateTime()!=null) {
				Assert.assertTrue(lastUpdateTime.after(metadata.getLastUpdateTime()));
			}
		}
		
	}
	
	public static void checkPropagationConstraint(PropagationConstraint propagationConstraint, PropagationConstraint gotPropagationConstraint) {
		Assert.assertTrue(propagationConstraint.getAddConstraint()==gotPropagationConstraint.getAddConstraint());
		Assert.assertTrue(propagationConstraint.getRemoveConstraint()==gotPropagationConstraint.getRemoveConstraint());
	}
	
	public static void checkConsistOf(ConsistsOf<? extends Resource, ? extends Facet> consistsOf, ConsistsOf<? extends Resource, ? extends Facet> gotConsistsOf) {
		checkUUIDAndMetadata(consistsOf, gotConsistsOf);
		
		if(consistsOf.getPropagationConstraint()==null) {
			PropagationConstraint propagationConstraint = gotConsistsOf.getPropagationConstraint();
			Assert.assertTrue(propagationConstraint.getAddConstraint()==AddConstraint.propagate);
			Assert.assertTrue(propagationConstraint.getRemoveConstraint()==RemoveConstraint.cascade);
			checkPropagationConstraint(propagationConstraint, gotConsistsOf.getPropagationConstraint());
		}else {
			checkPropagationConstraint(consistsOf.getPropagationConstraint(), gotConsistsOf.getPropagationConstraint());
		}
		
		Map<String, Object> additionalProperties = new HashMap<>(consistsOf.getAdditionalProperties());
		additionalProperties.remove(ModelElement.SUPERTYPES_PROPERTY);
		Map<String, Object> gotAdditionalProperties = new HashMap<>(gotConsistsOf.getAdditionalProperties());
		gotAdditionalProperties.remove(ModelElement.SUPERTYPES_PROPERTY);
		Assert.assertTrue(additionalProperties.size()==gotAdditionalProperties.size());
		for(String key : additionalProperties.keySet()) {
			Assert.assertTrue(gotAdditionalProperties.containsKey(key));
			Object additionalProperty = additionalProperties.get(key);
			Object gotAdditionalProperty = gotAdditionalProperties.get(key);
			Assert.assertTrue(additionalProperty.getClass() == gotAdditionalProperty.getClass());
			Assert.assertTrue(additionalProperty.equals(gotAdditionalProperty));
		}
	}
	
	public static void checkFacet(Facet facet, Facet gotFacet) throws Exception {
		checkUUIDAndMetadata(facet, gotFacet);
		Class<? extends Facet> clz = facet.getClass();
		Class<? extends Facet> gotClz = gotFacet.getClass();
		Assert.assertTrue(clz==gotClz);
		
		if(clz == SoftwareFacet.class) {
			checkSoftwareFacetAssertion((SoftwareFacet) facet, (SoftwareFacet) gotFacet);
		}
	}
	
	protected static <R extends Resource> void checkResource(R resource, R gotResource) throws Exception {
		Assert.assertTrue(resource.getClass() == gotResource.getClass());
		checkUUIDAndMetadata(resource, gotResource);
		
		List<ConsistsOf<? extends Resource, ? extends Facet>> resourceConsistsOf = resource.getConsistsOf();
		List<ConsistsOf<? extends Resource, ? extends Facet>> gotResourceConsistsOf = gotResource.getConsistsOf();
		Assert.assertTrue(resourceConsistsOf.size() == gotResourceConsistsOf.size());
		
		for(ConsistsOf<? extends Resource, ? extends Facet> consistsOf : resourceConsistsOf) {
			@SuppressWarnings("unchecked")
			ConsistsOf<? extends Resource, ? extends Facet> gotConsistsOf = (ConsistsOf<? extends Resource, ? extends Facet>) gotResource.getConsistsOf(consistsOf.getClass(), consistsOf.getTarget().getClass()).get(0);
			checkConsistOf(consistsOf, gotConsistsOf);
			
			Facet facet = consistsOf.getTarget();
			Facet gotFacet = gotConsistsOf.getTarget();
			checkFacet(facet, gotFacet);
		}
		
	}
	
	public <R extends Resource> R createResource(R r) throws Exception {
		R createdR  = resourceRegistryPublisher.create(r);
				
		checkResource(r, createdR);
		
		return createdR;
	}
	
	public EService createEService() throws Exception {
		EService eService = ERManagementTest.instantiateValidEService();
		return createResource(eService);
	}
	
	public HostingNode createHostingNode() throws Exception {
		return createHostingNode(null);
	}
	
	public HostingNode createHostingNode(EService eService) throws Exception {
		return createHostingNode(eService, RemoveConstraint.cascade);
	}
	
	public HostingNode createHostingNode(EService eService, RemoveConstraint removeConstraint) throws Exception {
		HostingNode hostingNode = ERManagementTest.instantiateValidHostingNode();
		if(eService!=null) {
			PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
			propagationConstraint.setRemoveConstraint(removeConstraint);
			Activates<HostingNode, EService> activates = new ActivatesImpl<HostingNode, EService>(hostingNode, eService,
					propagationConstraint);
			hostingNode.attachResource(activates);
		}
		return createResource(hostingNode);
	}
	
	public Configuration createConfiguration() throws Exception {
		Configuration configuration = ERManagementTest.instantiateValidConfiguration();
		return createResource(configuration);
	}
	
	public Map<String, Resource> createHostingNodeAndEService() throws Exception {
		Map<String, Resource> map = new HashMap<>();

		EService eService = createEService();
		map.put(EService.NAME, eService);

		HostingNode hostingNode = createHostingNode(eService);
		map.put(HostingNode.NAME, hostingNode);

		return map;
	}
	
	public <R extends Resource> void deleteResource(R r) throws Exception {
		if(r!=null) {
			resourceRegistryPublisher.delete(r);
		}
	}
	
	
	@Test
	public void testCreateEService() throws Exception {
		EService eService = null;
		try {
			eService = createEService();
		}finally {
			deleteResource(eService);
		}
	}

	/*
	@Test
	public void testReadResource() throws Exception {
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setUUID(UUIDUtility.fromString("26da57ee-33bd-4c4b-8aef-9206b61c329e"));
		String read = resourceManagement.read().toString();
		logger.debug(read);
	}
	*/
	
	/*
	@Test
	public void testDeleteResource() throws Exception {
		ResourceManagement resourceManagement = new ResourceManagement();
		resourceManagement.setUUID(UUIDUtility.fromString("64635295-7ced-4931-a55f-40fc8199b280"));
		boolean deleted = resourceManagement.delete();
		Assert.assertTrue(deleted);
	}
	*/
	
	@Test
	public void testCreateHostingNode() throws Exception {
		HostingNode hostingNode = null;
		try {
			hostingNode = createHostingNode();
		}finally {
			deleteResource(hostingNode);
		}
		
	}
	
	@Test
	public void testCreateHostingNodeAndEService() throws Exception {
		Map<String, Resource> map = createHostingNodeAndEService();
		deleteResource(map.get(EService.NAME));
		deleteResource(map.get(HostingNode.NAME));
	}
	
	@Test
	public void testCreateReadUpdateDeleteFacet() throws Exception {
		EService eService = createEService();
		
		try {
			CPUFacet cpuFacet = new CPUFacetImpl();
			cpuFacet.setClockSpeed("1 GHz");
			cpuFacet.setModel("Opteron");
			cpuFacet.setVendor("AMD");
			
			ConsistsOf<EService, CPUFacet> consistsOf = new ConsistsOfImpl<EService, CPUFacet>(eService, cpuFacet);
			
			ConsistsOf<EService, CPUFacet> createdConsistsOf = resourceRegistryPublisher.createConsistsOf(consistsOf);
			
			CPUFacet createdCpuFacet = createdConsistsOf.getTarget();
			
			Assert.assertTrue(cpuFacet.getClockSpeed().compareTo(createdCpuFacet.getClockSpeed()) == 0);
			Assert.assertTrue(cpuFacet.getModel().compareTo(createdCpuFacet.getModel()) == 0);
			Assert.assertTrue(cpuFacet.getVendor().compareTo(createdCpuFacet.getVendor()) == 0);
	
			UUID uuid = createdCpuFacet.getID();
	
			CPUFacet readCpuFacet = resourceRegistryPublisher.read(createdCpuFacet.getClass(), uuid);
			Assert.assertTrue(cpuFacet.getClockSpeed().compareTo(readCpuFacet.getClockSpeed()) == 0);
			Assert.assertTrue(cpuFacet.getModel().compareTo(readCpuFacet.getModel()) == 0);
			Assert.assertTrue(cpuFacet.getVendor().compareTo(readCpuFacet.getVendor()) == 0);
			Assert.assertTrue(uuid.compareTo(readCpuFacet.getID()) == 0);
	
			String newVendor = "Intel";
			String newClockSpeed = "2 GHz";
			readCpuFacet.setVendor(newVendor);
			readCpuFacet.setClockSpeed(newClockSpeed);
	
			String additionPropertyKey = "My";
			String additionPropertyValue = "Test";
			readCpuFacet.setAdditionalProperty(additionPropertyKey, additionPropertyValue);
	
			CPUFacet updatedCpuFacet = resourceRegistryPublisher.update(readCpuFacet);
			Assert.assertTrue(updatedCpuFacet.getClockSpeed().compareTo(newClockSpeed) == 0);
			Assert.assertTrue(readCpuFacet.getModel().compareTo(updatedCpuFacet.getModel()) == 0);
			Assert.assertTrue(updatedCpuFacet.getVendor().compareTo(newVendor) == 0);
			Assert.assertTrue(((String) updatedCpuFacet.getAdditionalProperty(additionPropertyKey))
					.compareTo((String) readCpuFacet.getAdditionalProperty(additionPropertyKey)) == 0);
			Assert.assertTrue(uuid.compareTo(updatedCpuFacet.getID()) == 0);
			String user = ContextTest.getUser();
			Assert.assertTrue(updatedCpuFacet.getMetadata().getLastUpdateBy().compareTo(user) == 0);
	
			CPUFacet readUpdatedCpuFacet = resourceRegistryPublisher.read(updatedCpuFacet);
			
			Assert.assertTrue(updatedCpuFacet.getClockSpeed().compareTo(readUpdatedCpuFacet.getClockSpeed()) == 0);
			Assert.assertTrue(updatedCpuFacet.getModel().compareTo(readUpdatedCpuFacet.getModel()) == 0);
			Assert.assertTrue(updatedCpuFacet.getVendor().compareTo(readUpdatedCpuFacet.getVendor()) == 0);
			Assert.assertTrue(((String) updatedCpuFacet.getAdditionalProperty(additionPropertyKey))
					.compareTo((String) readUpdatedCpuFacet.getAdditionalProperty(additionPropertyKey)) == 0);
			Assert.assertTrue(uuid.compareTo(updatedCpuFacet.getID()) == 0);
	
			resourceRegistryPublisher.delete(readCpuFacet);
		} finally {
			deleteResource(eService);
		}
	}

	@Test
	public void testCreatePropertyTypeNotInSchema() throws Exception {
		/*
		 * A facet cannot be created per se. Going to create a Configuration which does
		 * not impose any particular constraint except the IdentifierFact
		 */
		Configuration configuration = instantiateValidConfiguration();
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");

		ValueSchema authorization = new ValueSchemaImpl();
		authorization.setValue("pwd");
		URI uri = new URI("https://www.gcube-system.org");
		authorization.setSchema(uri);
		accessPointFacet.setAuthorization(authorization);

		String additionlaPropertyKey = "Test";
		String additionlaPropertyValue = "MyTest";
		accessPointFacet.setAdditionalProperty(additionlaPropertyKey, additionlaPropertyValue);

		Encrypted encrypted = new EncryptedImpl();
		String plainValue = "my plain value";
		String encryptedValue = StringEncrypter.getEncrypter().encrypt(plainValue);
		encrypted.setValue(encryptedValue);
		String encryptedKey = "Enc";
		accessPointFacet.setAdditionalProperty(encryptedKey, encrypted);
		
		configuration.addFacet(accessPointFacet);

		String marshalled = ElementMapper.marshal(configuration);
		logger.debug(marshalled);

		Configuration createdConfiguration = createResource(configuration);

		AccessPointFacet apf = configuration.getFacets(AccessPointFacet.class).get(0);
		
		Assert.assertTrue(apf.getAuthorization() instanceof ValueSchema);
		Assert.assertTrue(apf.getAdditionalProperty(encryptedKey) instanceof Encrypted);
		Encrypted enc = (Encrypted) apf.getAdditionalProperty(encryptedKey);
		String encValue = enc.getValue();
		Assert.assertTrue(encValue.compareTo(encryptedValue) == 0);
		String decryptedValue = StringEncrypter.getEncrypter().decrypt(encValue);
		Assert.assertTrue(decryptedValue.compareTo(plainValue) == 0);
		Assert.assertTrue(((String) apf.getAdditionalProperty(additionlaPropertyKey)).compareTo(additionlaPropertyValue) == 0);

		deleteResource(createdConfiguration);
	}

	
	@Test
	public void testUpdateFacetValue() throws Exception {
		EService eService =null;
		try {
			eService = createEService(); 
					
			final String newVersion = "1.2.0";
			eService.getFacets(SoftwareFacet.class).get(0).setVersion(newVersion);
	
			EService updatedEService = resourceRegistryPublisher.update(eService);
			
			checkResource(eService, updatedEService);
			
			SoftwareFacet softwareFacet = updatedEService.getFacets(SoftwareFacet.class).get(0);
			Assert.assertTrue(softwareFacet.getVersion().compareTo(newVersion) == 0);
			
		}finally {
			deleteResource(eService);
		}
	}

//	public static final String TEST_RESOURCE = "test-resource.json";
//
//	// @Test
//	public void testUpdateResourceFromFile()
//			throws JsonParseException, JsonMappingException, IOException, ResourceRegistryException {
//		File file = new File("src/test/resources/" + TEST_RESOURCE);
//
//		logger.debug("{}", file.getAbsolutePath());
//
//		FileInputStream fileInputStream = new FileInputStream(file);
//		EService eService = ElementMapper.unmarshal(EService.class, fileInputStream);
//
//		ResourceManagement resourceManagement = new ResourceManagement();
//		resourceManagement.setUUID(eService.getUUID());
//		resourceManagement.setJson(ElementMapper.marshal(eService));
//
//		resourceManagement.update();
//
//	}
	
//	@Test
//	public void testCreateResourceFromJson() throws Exception {
//		String json = "{\"type\":\"VirtualService\",\"consistsOf\":[{\"type\":\"IsIdentifiedBy\",\"target\":{\"name\":\"aaaaaa\",\"optional\":\"true\",\"group\":\"xxxxx\",\"description\":\"\",\"version\":\"cvcvvv\",\"qualifier\":\"\",\"type\":\"SoftwareFacet\",\"aaaa\":\"dddddd\"}}]}";
//		String ret = resourceRegistryPublisher.create(json);
//	}

}
