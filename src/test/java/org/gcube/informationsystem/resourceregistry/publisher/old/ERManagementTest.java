/**
 * 
 */
package org.gcube.informationsystem.resourceregistry.publisher.old;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.informationsystem.base.reference.Direction;
import org.gcube.informationsystem.model.impl.properties.EncryptedImpl;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.DeleteConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.resourceregistry.api.exceptions.ResourceRegistryException;
import org.gcube.informationsystem.resourceregistry.api.exceptions.types.SchemaViolationException;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientImpl;
import org.gcube.informationsystem.resourceregistry.publisher.ContextTest;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisher;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisherFactory;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisherImpl;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.resourcemanagement.model.impl.entities.facets.AccessPointFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.EventFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.LicenseFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.MemoryFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.NetworkingFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.SoftwareFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.StateFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.EServiceImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasPersistentMemoryImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.HasVolatileMemoryImpl;
import org.gcube.resourcemanagement.model.impl.relations.consistsof.IsIdentifiedByImpl;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.AccessPointFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.EventFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.LicenseFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.MemoryFacet.MemoryUnit;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.SoftwareFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.StateFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.GCubeResource;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasPersistentMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasVolatileMemory;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.IsIdentifiedBy;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ERManagementTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ERManagementTest.class);
	
	public static final String PASSWORD = "PASSWORD";
	
	protected ResourceRegistryPublisher resourceRegistryPublisher;
	protected ResourceRegistryClient resourceRegistryClient;
	
	public ERManagementTest() {
		Object rrURLOBj = ContextTest.properties.get(RESOURCE_REGISTRY_URL_PROPERTY);
		if(rrURLOBj!=null  && !rrURLOBj.toString().isEmpty()) {
			resourceRegistryPublisher = new ResourceRegistryPublisherImpl(rrURLOBj.toString());
			resourceRegistryClient = new ResourceRegistryClientImpl(rrURLOBj.toString());
		}else {
			resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
			resourceRegistryClient = ResourceRegistryClientFactory.create();
		}
	}
	
	@Test
	public void testEncrypted() throws Exception {
		EService eService = new EServiceImpl();
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		URL endpoint = new URL("https://d4science.org:443");
		accessPointFacet.setEndpoint(endpoint.toURI());
		Encrypted encrypted = new EncryptedImpl();
		encrypted.setValue(StringEncrypter.getEncrypter().encrypt("1"));
		accessPointFacet.setAdditionalProperty(PASSWORD, encrypted);
		eService.addFacet(accessPointFacet);

		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		
		IsIdentifiedBy<Resource,Facet> isIdentifiedBy = new IsIdentifiedByImpl<Resource,Facet>(eService, softwareFacet,
				null);
		eService.addFacet(isIdentifiedBy);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		ResourceRegistryPublisher resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
		EService receivedEservice = resourceRegistryPublisher.create(eService);
		
		AccessPointFacet receivedFacet = (AccessPointFacet) receivedEservice.getFacets(AccessPointFacet.class).get(0);
		UUID eServiceUUID = receivedEservice.getID();
		UUID facetUUID = receivedFacet.getID();
		
		logger.debug("Created {} : {}", EService.NAME, ElementMapper.marshal(receivedEservice));
		
		for(int i=0; i<10; i++) {
			try {
				if(i%2==0) {
					receivedEservice = resourceRegistryPublisher.update(receivedEservice);
					logger.debug("Updated {} : {}", EService.NAME, ElementMapper.marshal(receivedEservice));
				}else {
					receivedEservice = resourceRegistryPublisher.read(receivedEservice);
					logger.debug("Read {} : {}", EService.NAME, ElementMapper.marshal(receivedEservice));
				}
				UUID receivedEServiceUUID = receivedEservice.getID();
				Assert.assertTrue(eServiceUUID.compareTo(receivedEServiceUUID)==0);
				
				receivedFacet = (AccessPointFacet) receivedEservice.getFacets(AccessPointFacet.class).get(0);
				UUID receivedFacetUUID = receivedFacet.getID();
				Assert.assertTrue(facetUUID.compareTo(receivedFacetUUID)==0);
				
				Encrypted receivedEncrypted = (Encrypted) receivedFacet.getAdditionalProperty(PASSWORD);
				Assert.assertTrue(encrypted.getValue().compareTo(receivedEncrypted.getValue())==0);
			} catch(ResourceRegistryException e) {
				logger.error("", e);
			}
		}
		
		boolean deleted = resourceRegistryPublisher.delete(receivedEservice);
		Assert.assertTrue(deleted);
	}
	
	@Test
	public void testCreateDeleteEService() throws Exception {
		EService eService = new EServiceImpl();
		
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		IsIdentifiedBy<EService,Facet> isIdentifiedBy = new IsIdentifiedByImpl<EService,Facet>(eService, softwareFacet,
				null);
		eService.addFacet(isIdentifiedBy);
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		LicenseFacet licenseFacet = new LicenseFacetImpl();
		licenseFacet.setName("EUPL");
		licenseFacet.setTextURL(
				new URL("https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11"));
		eService.addFacet(licenseFacet);
		
		eService = resourceRegistryPublisher.createResource(eService);
		
		boolean deleted = resourceRegistryPublisher.deleteResource(eService);
		Assert.assertTrue(deleted);
	}
	
	@Test
	public void testCreateUpdateDeleteEService() throws Exception {
		EService eService = new EServiceImpl();
		
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		IsIdentifiedBy<EService,Facet> isIdentifiedBy = new IsIdentifiedByImpl<EService,Facet>(eService, softwareFacet,
				null);
		eService.addFacet(isIdentifiedBy);
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		LicenseFacet licenseFacet = new LicenseFacetImpl();
		licenseFacet.setName("EUPL");
		licenseFacet.setTextURL(
				new URL("https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11"));
		eService.addFacet(licenseFacet);
		
		eService = resourceRegistryPublisher.createResource(eService);
		
		eService.getFacets(SoftwareFacet.class).get(0).setVersion("1.2.0");
		
		eService = resourceRegistryPublisher.updateResource(eService);
		
		Assert.assertTrue(eService.getFacets(SoftwareFacet.class).get(0).getVersion().compareTo("1.2.0") == 0);
		
		boolean deleted = resourceRegistryPublisher.deleteResource(eService);
		Assert.assertTrue(deleted);
	}
	
	@Test
	public void testCreateEServiceAsJsonString() throws Exception {
		EService eService = new EServiceImpl();
		
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		IsIdentifiedBy<EService,Facet> isIdentifiedBy = new IsIdentifiedByImpl<EService,Facet>(eService, softwareFacet,
				null);
		eService.addFacet(isIdentifiedBy);
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		LicenseFacet licenseFacet = new LicenseFacetImpl();
		licenseFacet.setName("EUPL");
		licenseFacet.setTextURL(
				new URL("https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11"));
		eService.addFacet(licenseFacet);
		
		String json = ElementMapper.marshal(eService);
		
		String createdString = resourceRegistryPublisher.createResource(json);
		
		EService created = ElementMapper.unmarshal(EService.class, createdString);
		
		boolean deleted = resourceRegistryPublisher.deleteResource(created);
		Assert.assertTrue(deleted);
	}
	
	@Test
	public void testCreateUpdateDeleteEServiceAsJsonString() throws Exception {
		EService eService = new EServiceImpl();
		
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		IsIdentifiedBy<EService,Facet> isIdentifiedBy = new IsIdentifiedByImpl<EService,Facet>(eService, softwareFacet,
				null);
		eService.addFacet(isIdentifiedBy);
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		LicenseFacet licenseFacet = new LicenseFacetImpl();
		licenseFacet.setName("EUPL");
		licenseFacet.setTextURL(
				new URL("https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11"));
		eService.addFacet(licenseFacet);
		
		eService = resourceRegistryPublisher.createResource(eService);
		
		eService.getFacets(SoftwareFacet.class).get(0).setVersion("1.2.0");
		
		eService = resourceRegistryPublisher.updateResource(eService);
		
		Assert.assertTrue(eService.getFacets(SoftwareFacet.class).get(0).getVersion().compareTo("1.2.0") == 0);
		
		boolean deleted = resourceRegistryPublisher.deleteResource(eService);
		Assert.assertTrue(deleted);
	}
	
	public Map<String,Resource> createHostingNodeAndEService() throws Exception {
		Map<String,Resource> map = new HashMap<>();
		
		EService eService = new EServiceImpl();
		
		SoftwareFacet softwareFacet = new SoftwareFacetImpl();
		softwareFacet.setGroup("InformationSystem");
		softwareFacet.setName("resource-registry");
		softwareFacet.setVersion("1.1.0");
		
		IsIdentifiedBy<Resource,Facet> isIdentifiedBy = new IsIdentifiedByImpl<Resource,Facet>(eService, softwareFacet,
				null);
		eService.addFacet(isIdentifiedBy);
		
		AccessPointFacet accessPointFacet = new AccessPointFacetImpl();
		accessPointFacet.setEndpoint(new URI("https://localhost"));
		accessPointFacet.setEntryName("port1");
		eService.addFacet(accessPointFacet);
		
		EventFacet eventFacet = new EventFacetImpl();
		eventFacet.setDate(Calendar.getInstance().getTime());
		eventFacet.setEvent("Created");
		eService.addFacet(eventFacet);
		
		StateFacet stateFacet = new StateFacetImpl();
		stateFacet.setValue("ready");
		eService.addFacet(stateFacet);
		
		eService = resourceRegistryPublisher.createResource(eService);
		map.put(EService.NAME, eService);
		
//		networkingFacet = resourceRegistryPublisher.createFacet(networkingFacet);
//		logger.debug("Unmarshalled {} {}", NetworkingFacet.NAME, networkingFacet);
		
		HostingNode hostingNode = new HostingNodeImpl();

		NetworkingFacet networkingFacet = new NetworkingFacetImpl();
		networkingFacet.setIPAddress("146.48.87.183");
		networkingFacet.setHostName("pc-frosini.isti.cnr.it");
		networkingFacet.setDomainName("isti.cnr.it");
		networkingFacet.setMask("255.255.248.0");
		networkingFacet.setBroadcastAddress("146.48.87.255");

		isIdentifiedBy = new IsIdentifiedByImpl<Resource,Facet>(hostingNode, networkingFacet, null);
		hostingNode.addFacet(isIdentifiedBy);

		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		hostingNode.addFacet(cpuFacet);
		
		MemoryFacet ram = new MemoryFacetImpl();
		ram.setSize(8);
		ram.setUnit(MemoryUnit.GB);
		ram.setUsed(2);
		
		HasVolatileMemory<HostingNode, MemoryFacet> hasVolatileMemory = 
				new HasVolatileMemoryImpl<HostingNode, MemoryFacet>(hostingNode, ram, null);
		hostingNode.addFacet(hasVolatileMemory);
		
		
		MemoryFacet disk = new MemoryFacetImpl();
		disk.setSize(256);
		disk.setUnit(MemoryUnit.GB);
		disk.setUsed(120);
		
		HasPersistentMemory<HostingNode, MemoryFacet> hasPersistentMemory = 
				new HasPersistentMemoryImpl<HostingNode, MemoryFacet>(hostingNode, disk, null);
		hostingNode.addFacet(hasPersistentMemory);
		
		StateFacet hnStateFacet = new StateFacetImpl();
		hnStateFacet.setValue("certified");
		hostingNode.addFacet(hnStateFacet);

		EventFacet hnEventFacet = new EventFacetImpl();
		hnEventFacet.setDate(Calendar.getInstance().getTime());
		hnEventFacet.setEvent("Created");
		hostingNode.addFacet(hnEventFacet);
		
		PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setDeleteConstraint(DeleteConstraint.cascade);
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascade);
		propagationConstraint.setAddConstraint(AddConstraint.unpropagate);
		
		Activates<HostingNode,EService> activates = new ActivatesImpl<HostingNode,EService>(hostingNode, eService,
				propagationConstraint);
		hostingNode.attachResource(activates);
		
		hostingNode = resourceRegistryPublisher.createResource(hostingNode);
		logger.debug("Unmarshalled {} {}", HostingNode.NAME, hostingNode);
		map.put(HostingNode.NAME, hostingNode);
		
		return map;
	}
	
	@Test
	public void testCreateHostingNodeAndEService() throws Exception {
		Map<String,Resource> map = createHostingNodeAndEService();
		
		boolean deleted = resourceRegistryPublisher.deleteResource(map.get(EService.NAME));
		Assert.assertTrue(deleted);
		
		deleted = resourceRegistryPublisher.deleteResource(map.get(HostingNode.NAME));
		Assert.assertTrue(deleted);
	}
	
	@Test
	public void testCreateHostingNodeAndEServiceWithSharedFacet() throws Exception {
		Map<String,Resource> map = createHostingNodeAndEService();
		
		EService eService = (EService) map.get(EService.NAME);
		HostingNode hostingNode = (HostingNode) map.get(HostingNode.NAME);
		
		Facet shared = hostingNode.getConsistsOf().get(0).getTarget();
		UUID sharedFacetUUID = shared.getID();
		
		try {
			ConsistsOf<EService,Facet> consistsOf = new ConsistsOfImpl<EService,Facet>(eService, shared, null);
			consistsOf = resourceRegistryPublisher.createConsistsOf(consistsOf);
		} catch (SchemaViolationException e) {
			logger.debug("As expected ", e);
		} finally {
			boolean deleted = resourceRegistryPublisher.deleteResource(map.get(EService.NAME));
			Assert.assertTrue(deleted);
			
			deleted = resourceRegistryPublisher.deleteResource(map.get(HostingNode.NAME));
			Assert.assertTrue(deleted);
			
			try {
				resourceRegistryClient.getInstance(Facet.class, sharedFacetUUID);
				throw new Exception(String.format("Shared Facet %s was not deleted", shared));
			} catch(ResourceRegistryException e) {
				logger.debug("Shared Facet was not found as expected");
			}
		}
		
	}
	
	@Test
	public void testGetAll() throws Exception {
		Map<String,List<Resource>> resources = new HashMap<>();
		
		final int MAX = 5;
		int typeNumber = 0;
		
		for(int i = 0; i < MAX; i++) {
			Map<String,Resource> map = createHostingNodeAndEService();
			if(typeNumber == 0) {
				typeNumber = map.size();
			}
			for(String key : map.keySet()) {
				if(!resources.containsKey(key)) {
					resources.put(key, new ArrayList<Resource>());
				}
				resources.get(key).add(map.get(key));
			}
		}
		
		/* Getting all instances of created specific Resources */
		for(String key : resources.keySet()) {
			String instances = resourceRegistryClient.getInstances(key, false);
			List<Resource> list = ElementMapper.unmarshalList(Resource.class, instances);
			logger.debug("{} are {} : {} ", key, list.size(), list);
			Assert.assertTrue(list.size() == MAX);
		}
		
		/* Getting all Resources polymorphic and non polymorphic */
		List<Resource> list = resourceRegistryClient.getInstances(Resource.class, true);
		logger.debug("{} are {} : {} ", Resource.NAME, list.size(), list);
		Assert.assertTrue(list.size() == (MAX * typeNumber));
		
		list = resourceRegistryClient.getInstances(Resource.class, false);
		Assert.assertTrue(list.size() == 0);
		
		/* Getting all IsRelatedTo polymorphic and non polymorphic */
		
		@SuppressWarnings("rawtypes")
		List<IsRelatedTo> resourcesList = resourceRegistryClient.getInstances(IsRelatedTo.class, true);
		logger.debug("{} are {} : {} ", IsRelatedTo.NAME, resourcesList.size(), resourcesList);
		Assert.assertTrue(resourcesList.size() == MAX);
		
		resourcesList = resourceRegistryClient.getInstances(IsRelatedTo.class, false);
		Assert.assertTrue(resourcesList.size() == 0);
		
		/* Getting all ConsistsOf polymorphic and non polymorphic */
		
		@SuppressWarnings("rawtypes")
		List<ConsistsOf> consistsOfPolimorphicList = resourceRegistryClient.getInstances(ConsistsOf.class, true);
		logger.debug("{} are {} : {} ", IsRelatedTo.NAME, consistsOfPolimorphicList.size(), consistsOfPolimorphicList);
		
		@SuppressWarnings("rawtypes")
		List<ConsistsOf> consistsOfNonPolimorphicList = resourceRegistryClient.getInstances(ConsistsOf.class, false);
		logger.debug("{} are {} : {} ", IsRelatedTo.NAME, consistsOfNonPolimorphicList.size(),
				consistsOfNonPolimorphicList);
		
		Assert.assertTrue(consistsOfPolimorphicList.size() >= consistsOfNonPolimorphicList.size());
		
		/* Removing created Entity and Relation to have a clean DB */
		
		List<Resource> resourceList = resources.get(HostingNode.NAME);
		for(Resource r : resourceList) {
			boolean deleted = resourceRegistryPublisher.deleteResource(r);
			Assert.assertTrue(deleted);
		}
	}
	
	@Test
	public void testGetAllFrom() throws Exception {
		Map<String,Resource> map = createHostingNodeAndEService();
		
		EService eService = (EService) map.get(EService.NAME);
		UUID eServiceUUID = eService.getID();
		
		HostingNode hostingNode = (HostingNode) map.get(HostingNode.NAME);
		UUID hostingNodeUUID = hostingNode.getID();
		
		/* EService */
		List<Resource> resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, EService.class, eServiceUUID, Direction.BOTH, true);
		Assert.assertTrue(resourceList.size() == 1);
		Resource resource = resourceList.get(0);
		Assert.assertTrue(resource.getID().compareTo(hostingNodeUUID) == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, EService.class, eServiceUUID, Direction.OUT, true);
		Assert.assertTrue(resourceList.size() == 1);
		resource = resourceList.get(0);
		Assert.assertTrue(resource.getID().compareTo(hostingNodeUUID) == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, EService.class, eServiceUUID, Direction.IN, true);
		Assert.assertTrue(resourceList.size() == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, EService.class, eServiceUUID, Direction.BOTH, false);
		Assert.assertTrue(resourceList.size() == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, EService.class, eServiceUUID, Direction.OUT, false);
		Assert.assertTrue(resourceList.size() == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, EService.class, eServiceUUID, Direction.IN, false);
		Assert.assertTrue(resourceList.size() == 0);
		/* END EService */
		
		/* Hosting Node */
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, HostingNode.class, hostingNodeUUID, Direction.BOTH, true);
		Assert.assertTrue(resourceList.size() == 1);
		resource = resourceList.get(0);
		Assert.assertTrue(resource.getID().compareTo(eServiceUUID) == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, HostingNode.class, hostingNodeUUID, Direction.OUT, true);
		Assert.assertTrue(resourceList.size() == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, HostingNode.class, hostingNodeUUID, Direction.IN, true);
		Assert.assertTrue(resourceList.size() == 1);
		resource = resourceList.get(0);
		Assert.assertTrue(resource.getID().compareTo(eServiceUUID) == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, HostingNode.class, hostingNodeUUID, Direction.BOTH, false);
		Assert.assertTrue(resourceList.size() == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, HostingNode.class, hostingNodeUUID, Direction.OUT, false);
		Assert.assertTrue(resourceList.size() == 0);
		
		resourceList = resourceRegistryClient.getRelatedResourcesFromReferenceResource(Resource.class,
				IsRelatedTo.class, HostingNode.class, hostingNodeUUID, Direction.IN, false);
		Assert.assertTrue(resourceList.size() == 0);
		/* END HostingNode */
		
		Facet identificationFacet = eService.getIdentificationFacets().get(0);
		UUID identificationFacetUUID = identificationFacet.getID();
		
		
		/* Eservice with Software Facet as Identification Facet */
		resourceList = resourceRegistryClient.getResourcesFromReferenceFacet(Resource.class, ConsistsOf.class, SoftwareFacet.class, identificationFacetUUID, true);
		Assert.assertTrue(resourceList.size() == 1);
		resource = resourceList.get(0);
		Facet targetIdentificationFacet = ((GCubeResource) resource).getIdentificationFacets().get(0);
		Assert.assertTrue(resource.getID().compareTo(eServiceUUID) == 0);
		Assert.assertTrue(targetIdentificationFacet.getID().compareTo(identificationFacetUUID) == 0);
		
		resourceList = resourceRegistryClient.getResourcesFromReferenceFacet(Resource.class, ConsistsOf.class, SoftwareFacet.class, identificationFacetUUID, false);
		Assert.assertTrue(resourceList.size() == 0);
		/* END Eservice with Software Facet as Identification Facet */
		
		
		
		/* END SoftwareFacet of Eservice */
		/* Support removed
		List<Facet> facets = resourceRegistryClient.getRelated(Facet.class, ConsistsOf.class, EService.class, eServiceUUID, Direction.in, true);
		Assert.assertTrue(resourceList.size() == 1);
		Facet facet = facets.get(0);
		Assert.assertTrue(facet.getUUID().compareTo(identificationFacetUUID) == 0);
		
		facets = resourceRegistryClient.getRelated(Facet.class, ConsistsOf.class, EService.class, eServiceUUID, Direction.in, false);
		Assert.assertTrue(resourceList.size() == 0);
		*/
		/* END SoftwareFacet of Eservice */
		
		/* Removing created Entity and Relation to have a clean DB */
		boolean deleted = resourceRegistryPublisher.deleteResource(hostingNode);
		Assert.assertTrue(deleted);
	}
	
	@Test
	public void testCreateHostingNodeThenActivatesWithEService() throws Exception {
		
		HostingNode hostingNode = org.gcube.informationsystem.resourceregistry.publisher.ERManagementTest.instantiateValidHostingNode();
		
		hostingNode = resourceRegistryPublisher.createResource(hostingNode);
		
		/* -------- */
		
		EService eService = org.gcube.informationsystem.resourceregistry.publisher.ERManagementTest.instantiateValidEService();
		
		PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setDeleteConstraint(DeleteConstraint.cascade);
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascade);
		propagationConstraint.setAddConstraint(AddConstraint.unpropagate);
		
		Activates<HostingNode,EService> activates = new ActivatesImpl<HostingNode,EService>(hostingNode, eService,
				propagationConstraint);
		
		activates = resourceRegistryPublisher.createIsRelatedTo(activates);
		hostingNode.attachResource(activates);
		
		logger.debug("Created {} is {}", Activates.NAME, ElementMapper.marshal(activates));
		
		boolean deleted = resourceRegistryPublisher.deleteResource(hostingNode);
		Assert.assertTrue(deleted);
		
	}
	
}
