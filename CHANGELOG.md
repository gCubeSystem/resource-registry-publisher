This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Resource Registry Publisher

## [v4.5.0-SNAPSHOT]

- Added support for paginated results [#24648]


## [v4.4.0]

- Migrated code to reorganized E/R format [#24992]


## [v4.3.0]

- Enhanced gcube-bom version
- Added usage of common-utility to overcome issues with different Smartgears version (i.e. 3 and 4)
- Added the possibility for a client to add additional HTTP headers
- Added the possibility to create a client instance by specifying context


## [v4.2.0]

- Aligned code to Luca Frosini dissertation theory 
- Add/Remove to/from Context return the list of affected instances and not just success or failure code [#20555]
- Aligned client with changes of Sharing REST collection redesign [#20530][#20555]
- Aligned overrided APIs [#21979]
- Getting all the relations of a type it is returned a list of relations and not the list of Resources sources of such relation [#22003]
- Added check for reserved UUID
- Added support for context names included in header among UUIDs [#22090]
- Client gets service URL using resource-registry-api lib utility [#23658]


## [v4.1.0] 

- Used ContextCache to make the client more efficient
- Added APIs to get instance contexts [#20013]
- Added support to request contexts in instances header [#20012]


## [v4.0.0] [r4.26.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30

- Refactored code to support IS Model reorganization (e.g naming, packages)
- Using gxREST in place of custom class of resource-registry-api [#11455]
- Refactored code to support renaming of Embedded class to Property [#13274]


## [v2.0.0] [r4.13.0] - 2018-11-20

- Using new resource-registry REST interface [#11903]


## [v1.5.0] [r4.9.0] - 2017-12-20

- Changed pom.xml to use new make-servicearchive directive [#10168]


## [v1.4.0] [r4.6.0] - 2017-07-25

- Added HTTPS support [#8757]
- Added HTTP Redirection support [#8757]


## [v1.3.0] [r4.5.0] - 2017-06-07

- Removed common-generic-clients, common-gcube-calls and common-fw-clients from dependencies
- The client is now a generic http-client (using HTTPCall class provided in API) auto-query the old is to get resource-registry instance. It is not implement as gcube-proxy anymore.
- Added exists() method which uses HEAD HTTP method to get existence from Resource Registry
- Added the method to create a relation and target entity with one call by using the new API provided from Resource Registry


## [v1.2.0] [r4.3.0] - 2017-03-16

- Added API Remove From Context
- Added ResourceRegistryException JSON deserialization


## [v1.1.0] [r4.2.0] - 2016-12-15

- Added API AddToContext
- Added API to support update Resource (with all facets)


## [v1.0.0] [r4.1.0] - 2016-11-07

- First Release

